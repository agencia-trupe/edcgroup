@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <h1>{{ t('404') }}</h1>
    </div>

@endsection
