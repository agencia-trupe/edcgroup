@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('banner', 'Banner') !!}
    @if($registro->banner)
    <img src="{{ url('assets/img/quem-somos/'.$registro->banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('banner', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('titulo_banner_pt', 'Título Banner PT') !!}
        {!! Form::text('titulo_banner_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_banner_en', 'Título Banner EN') !!}
        {!! Form::text('titulo_banner_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_banner_es', 'Título Banner ES') !!}
        {!! Form::text('titulo_banner_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_banner_pt', 'Texto Banner PT') !!}
        {!! Form::textarea('texto_banner_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_banner_en', 'Texto Banner EN') !!}
        {!! Form::textarea('texto_banner_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_banner_es', 'Texto Banner ES') !!}
        {!! Form::textarea('texto_banner_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('slogan_pt', 'Slogan PT') !!}
        {!! Form::text('slogan_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('slogan_en', 'Slogan EN') !!}
        {!! Form::text('slogan_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('slogan_es', 'Slogan ES') !!}
        {!! Form::text('slogan_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

<div class="row">
    <div class="form-group col-md-2" style="display:flex; flex-direction:column;">
        <label for="">Perfil Atuação 1 - Ícone</label>
        <img src="{{ asset('assets/img/layout/icone-perfil-1.svg') }}" alt="" style="width:auto; max-width:80%; margin-top:10px;">
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('item1_atuacao_pt', 'Perfil Atuação 1 PT') !!}
        {!! Form::textarea('item1_atuacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item1_atuacao_en', 'Perfil Atuação 1 EN') !!}
        {!! Form::textarea('item1_atuacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item1_atuacao_es', 'Perfil Atuação 1 ES') !!}
        {!! Form::textarea('item1_atuacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2" style="display:flex; flex-direction:column;">
        <label for="">Perfil Atuação 2 - Ícone</label>
        <img src="{{ asset('assets/img/layout/icone-perfil-2.svg') }}" alt="" style="width:auto; max-width:80%; margin-top:10px;">
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('item2_atuacao_pt', 'Perfil Atuação 2 PT') !!}
        {!! Form::textarea('item2_atuacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item2_atuacao_en', 'Perfil Atuação 2 EN') !!}
        {!! Form::textarea('item2_atuacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item2_atuacao_es', 'Perfil Atuação 2 ES') !!}
        {!! Form::textarea('item2_atuacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2" style="display:flex; flex-direction:column;">
        <label for="">Perfil Atuação 3 - Ícone</label>
        <img src="{{ asset('assets/img/layout/icone-perfil-3.svg') }}" alt="" style="width:auto; max-width:80%; margin-top:10px;">
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('item3_atuacao_pt', 'Perfil Atuação 3 PT') !!}
        {!! Form::textarea('item3_atuacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item3_atuacao_en', 'Perfil Atuação 3 EN') !!}
        {!! Form::textarea('item3_atuacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item3_atuacao_es', 'Perfil Atuação 3 ES') !!}
        {!! Form::textarea('item3_atuacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2" style="display:flex; flex-direction:column;">
        <label for="">Perfil Atuação 4 - Ícone</label>
        <img src="{{ asset('assets/img/layout/icone-perfil-4.svg') }}" alt="" style="width:auto; max-width:80%; margin-top:10px;">
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('item4_atuacao_pt', 'Perfil Atuação 4 PT') !!}
        {!! Form::textarea('item4_atuacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item4_atuacao_en', 'Perfil Atuação 4 EN') !!}
        {!! Form::textarea('item4_atuacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item4_atuacao_es', 'Perfil Atuação 4 ES') !!}
        {!! Form::textarea('item4_atuacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-2" style="display:flex; flex-direction:column;">
        <label for="">Perfil Atuação 5 - Ícone</label>
        <img src="{{ asset('assets/img/layout/icone-perfil-5.svg') }}" alt="" style="width:auto; max-width:80%; margin-top:10px;">
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('item5_atuacao_pt', 'Perfil Atuação 5 PT') !!}
        {!! Form::textarea('item5_atuacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item5_atuacao_en', 'Perfil Atuação 5 EN') !!}
        {!! Form::textarea('item5_atuacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('item5_atuacao_es', 'Perfil Atuação 5 ES') !!}
        {!! Form::textarea('item5_atuacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('video', 'Vídeo do YouTube') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="observacao" style="color: red; font-style: italic;">Incluir aqui somente a parte da url após o "v=". Exemplo: <span style="text-decoration: underline;">https://www.youtube.com/watch?v=<strong>M08wxK31Jzgs</strong></span></p>
</div>

<hr>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_missao_pt', 'Missão PT') !!}
        {!! Form::textarea('texto_missao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_missao_en', 'Missão EN') !!}
        {!! Form::textarea('texto_missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_missao_es', 'Missão ES') !!}
        {!! Form::textarea('texto_missao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_visao_pt', 'Visão PT') !!}
        {!! Form::textarea('texto_visao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_visao_en', 'Visão EN') !!}
        {!! Form::textarea('texto_visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_visao_es', 'Visão ES') !!}
        {!! Form::textarea('texto_visao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_valores_pt', 'Valores PT') !!}
        {!! Form::textarea('texto_valores_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_valores_en', 'Valores EN') !!}
        {!! Form::textarea('texto_valores_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_valores_es', 'Valores ES') !!}
        {!! Form::textarea('texto_valores_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<hr>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_metodologia_pt', 'Metodologia Própria PT') !!}
        {!! Form::textarea('texto_metodologia_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_metodologia_en', 'Metodologia Própria EN') !!}
        {!! Form::textarea('texto_metodologia_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_metodologia_es', 'Metodologia Própria ES') !!}
        {!! Form::textarea('texto_metodologia_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('frase1_certificacao_pt', 'Frase 1 Certificação PT') !!}
        {!! Form::text('frase1_certificacao_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase1_certificacao_en', 'Frase 1 Certificação EN') !!}
        {!! Form::text('frase1_certificacao_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase1_certificacao_es', 'Frase 1 Certificação ES') !!}
        {!! Form::text('frase1_certificacao_es', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('frase2_certificacao_pt', 'Frase 2 Certificação PT') !!}
        {!! Form::text('frase2_certificacao_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase2_certificacao_en', 'Frase 2 Certificação EN') !!}
        {!! Form::text('frase2_certificacao_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase2_certificacao_es', 'Frase 2 Certificação ES') !!}
        {!! Form::text('frase2_certificacao_es', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('frase3_certificacao_pt', 'Frase 3 Certificação PT') !!}
        {!! Form::text('frase3_certificacao_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase3_certificacao_en', 'Frase 3 Certificação EN') !!}
        {!! Form::text('frase3_certificacao_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase3_certificacao_es', 'Frase 3 Certificação ES') !!}
        {!! Form::text('frase3_certificacao_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('titulo_qualidade_pt', 'Título da Política da Qualidade PT') !!}
        {!! Form::textarea('titulo_qualidade_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_qualidade_en', 'Título da Política da Qualidade EN') !!}
        {!! Form::textarea('titulo_qualidade_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_qualidade_es', 'Título da Política da Qualidade ES') !!}
        {!! Form::textarea('titulo_qualidade_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('politica_qualidade_pt', 'Política da Qualidade PT') !!}
        {!! Form::textarea('politica_qualidade_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('politica_qualidade_en', 'Política da Qualidade EN') !!}
        {!! Form::textarea('politica_qualidade_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('politica_qualidade_es', 'Política da Qualidade ES') !!}
        {!! Form::textarea('politica_qualidade_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
    </div>
</div>

<hr>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('frase_estrutura_pt', 'Frase Estrutura PT') !!}
        {!! Form::text('frase_estrutura_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_estrutura_en', 'Frase Estrutura EN') !!}
        {!! Form::text('frase_estrutura_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_estrutura_es', 'Frase Estrutura ES') !!}
        {!! Form::text('frase_estrutura_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}