<ul class="nav navbar-nav" style="margin-left: 20px;">
    <li class="dropdown @if(Tools::routeIs(['painel.home*', 'painel.depoimentos*'])) active @endif">
        <a href="#" class="dropdown-toggle link-nav-painel" data-toggle="dropdown">Home <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}" class="link-nav-painel">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.depoimentos.*')) class="active" @endif>
                <a href="{{ route('painel.depoimentos.index') }}" class="link-nav-painel">Depoimentos</a>
            </li>
            <li @if(Tools::routeIs('painel.depoimentos-candidatos*')) class="active" @endif>
                <a href="{{ route('painel.depoimentos-candidatos.index') }}" class="link-nav-painel">Depoimentos Candidatos</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.empresas*')) class="active" @endif>
        <a href="{{ route('painel.empresas.index') }}" class="link-nav-painel">Empresas do Grupo</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.quem-somos*', 'painel.clientes*', 'painel.estrutura*'])) active @endif">
        <a href="#" class="dropdown-toggle link-nav-painel" data-toggle="dropdown">Quem Somos <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.quem-somos*')) class="active" @endif>
                <a href="{{ route('painel.quem-somos.index') }}" class="link-nav-painel">Quem Somos</a>
            </li>
            <li @if(Tools::routeIs('painel.clientes*')) class="active" @endif>
                <a href="{{ route('painel.clientes.index') }}" class="link-nav-painel">Clientes</a>
            </li>
            <li @if(Tools::routeIs('painel.estrutura*')) class="active" @endif>
                <a href="{{ route('painel.estrutura.index') }}" class="link-nav-painel">Estrutura</a>
            </li>
            <li @if(Tools::routeIs('painel.nossa-lideranca*')) class="active" @endif>
                <a href="{{ route('painel.nossa-lideranca.index') }}" class="link-nav-painel">Nossa Liderança</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.novidades*')) class="active" @endif>
        <a href="{{ route('painel.novidades.index') }}" class="link-nav-painel">Novidades</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.termos-de-uso*', 'painel.politica-de-privacidade*', 'painel.contratacao*'])) active @endif">
        <a href="#" class="dropdown-toggle link-nav-painel" data-toggle="dropdown">Textos <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.termos-de-uso*')) class="active" @endif>
                <a href="{{ route('painel.termos-de-uso.index') }}" class="link-nav-painel">Termos de Uso</a>
            </li>
            <li @if(Tools::routeIs('painel.politica-de-privacidade*')) class="active" @endif>
                <a href="{{ route('painel.politica-de-privacidade.index') }}" class="link-nav-painel">Política de Privacidade</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.contratacao*')) class="active" @endif>
                <a href="{{ route('painel.contratacao.index') }}" class="link-nav-painel">Contratação</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}" class="link-nav-painel">Newsletter</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.blog*'])) active @endif">
        <a href="#" class="dropdown-toggle link-nav-painel" data-toggle="dropdown">Blog <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.blog.pagina*')) class="active" @endif>
                <a href="{{ route('painel.blog.pagina.index') }}" class="link-nav-painel">Página</a>
            </li>
            <li @if(Tools::routeIs('painel.blog.posts*')) class="active" @endif>
                <a href="{{ route('painel.blog.posts.index') }}" class="link-nav-painel">Posts</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle link-nav-painel" data-toggle="dropdown">Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}" class="link-nav-painel">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}" class="link-nav-painel">Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
</ul>