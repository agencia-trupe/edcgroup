@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Candidatos /</small> Adicionar Depoimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.depoimentos-candidatos.store', 'files' => true]) !!}

        @include('painel.depoimentos-candidatos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
