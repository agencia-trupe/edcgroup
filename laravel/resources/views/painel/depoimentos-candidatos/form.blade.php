@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('depoimento_pt', 'Depoimento PT') !!}
    {!! Form::textarea('depoimento_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('depoimento_en', 'Depoimento EN') !!}
    {!! Form::textarea('depoimento_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('depoimento_es', 'Depoimento ES') !!}
    {!! Form::textarea('depoimento_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.depoimentos-candidatos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
