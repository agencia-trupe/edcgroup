@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Depoimentos Candidatos /</small> Editar Depoimento</h2>
    </legend>

    {!! Form::model($depoimento, [
        'route'  => ['painel.depoimentos-candidatos.update', $depoimento->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos-candidatos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
