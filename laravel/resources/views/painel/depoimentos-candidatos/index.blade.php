@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Depoimentos Candidatos
        <a href="{{ route('painel.depoimentos-candidatos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Depoimento</a>
    </h2>
</legend>


@if(!count($depoimentos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="depoimentos_candidatos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Nome</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($depoimentos as $depoimento)
        <tr class="tr-row" id="{{ $depoimento->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $depoimento->nome }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.depoimentos-candidatos.destroy', $depoimento->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.depoimentos-candidatos.edit', $depoimento->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection