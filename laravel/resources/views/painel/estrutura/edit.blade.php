@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estrutura /</small> Editar Estrutura</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.estrutura.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.estrutura.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
