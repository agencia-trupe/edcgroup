@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresas |</small> Editar Empresa</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['painel.empresas.update', $empresa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection