@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Empresas do Grupo
    </h2>
</legend>


@if(!count($empresas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="empresas">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Título</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($empresas as $empresa)
        <tr class="tr-row" id="{{ $empresa->id }}">
            <td><img src="{{ asset('assets/img/empresas/'.$empresa->capa) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $empresa->titulo_pt }}</td>
            <td>
                <a href="{{ route('painel.empresas.servicos.index', $empresa->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-list" style="margin-right:10px;"></span>Serviços
                </a>
            </td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.empresas.edit', $empresa->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection