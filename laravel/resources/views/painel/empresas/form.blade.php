@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresas/'.$empresa->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('titulo_pt', 'Título Empresa PT') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_en', 'Título Empresa EN') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_es', 'Título Empresa ES') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('link_website', 'Link Website') !!}
    {!! Form::text('link_website', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link_contato', 'Link Contato') !!}
    {!! Form::text('link_contato', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_pt', 'Texto PT') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_en', 'Texto EN') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_es', 'Texto ES') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoTitle']) !!}
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.empresas.index') }}" class="btn btn-default btn-voltar">Voltar</a>