@include('painel.common.flash')

<div class="row">
    <div class="col-md-12">
        <div class="well">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('home', 0) !!}
                    {!! Form::checkbox('home') !!}
                    HOME
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('titulo_servico_pt', 'Título Serviço PT') !!}
        {!! Form::text('titulo_servico_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_servico_en', 'Título Serviço EN') !!}
        {!! Form::text('titulo_servico_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_servico_es', 'Título Serviço ES') !!}
        {!! Form::text('titulo_servico_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.empresas.servicos.index', $empresa->id) }}" class="btn btn-default btn-voltar">Voltar</a>