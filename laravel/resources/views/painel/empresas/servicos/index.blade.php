@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.empresas.index') }}" title="Voltar para Empresas" class="btn btn-sm btn-default">&larr; Voltar para Empresas</a>

<legend>
    <h2>
        Empresa: {{ $empresa->titulo_pt }} | Serviços
        <a href="{{ route('painel.empresas.servicos.create', $empresa->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Serviço</a>
    </h2>
</legend>

@if(!count($servicos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="empresas_servicos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Título</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($servicos as $servico)
        <tr class="tr-row" id="{{ $servico->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $servico->titulo_servico_pt }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.empresas.servicos.destroy', $empresa->id, $servico->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.empresas.servicos.edit', [$empresa->id, $servico->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection