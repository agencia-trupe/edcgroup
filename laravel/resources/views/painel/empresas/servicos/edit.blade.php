@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa: {{ $empresa->titulo_pt }} | Serviços |</small> Editar Serviço</h2>
</legend>

{!! Form::model($servico, [
'route' => ['painel.empresas.servicos.update', $empresa->id, $servico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresas.servicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection