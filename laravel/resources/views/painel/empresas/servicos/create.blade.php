@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa: {{ $empresa->titulo_pt }} | Serviços |</small> Adicionar Serviço</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['painel.empresas.servicos.store', $empresa->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.empresas.servicos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection