@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('novidades_categoria_id', 'Categoria') !!}
    {!! Form::select('novidades_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('idioma', 'Idioma') !!}
    {!! Form::select('idioma', ['pt' => 'Português', 'en' => 'Inglês', 'es' => 'Espanhol'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'novidades']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/novidades/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('publicado', 0) !!}
            {!! Form::checkbox('publicado') !!}
            Publicado
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.novidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
