@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Novidades
            <div class="pull-right btn-group">
                <a href="{{ route('painel.novidades.categorias.index') }}" class="btn btn-primary btn-sm btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.novidades.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Novidade</a>
            </div>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        {!! Form::open(['route' => 'painel.novidades.index', 'method' => 'GET']) !!}

        <div class="form-group col-sm-2">
            {!! Form::select('idioma', ['pt' => 'Português', 'en' => 'Inglês', 'es' => 'Espanhol'], Request::get('idioma'), ['class' => 'form-control', 'placeholder' => 'Todos idiomas']) !!}
        </div>
        <div class="form-group col-sm-3" style="padding-left:0">
            {!! Form::select('categoria', $categorias, Request::get('categoria'), ['class' => 'form-control', 'placeholder' => 'Todas categorias']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
            <button class="btn btn-info" type="submit">
                <span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
                Filtrar
            </button>
        </div>

        {!! Form::close() !!}
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Data</th>
                <th>Idioma</th>
                <th>Publicado</th>
                <th>Título</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    @if($registro->categoria)
                    {{ $registro->categoria->titulo_pt }}
                    @else
                    <span class="label label-warning">sem categoria</span>
                    @endif
                </td>
                <td>{{ $registro->data }}</td>
                <td>{{ ['pt' => 'Português', 'en' => 'Inglês', 'es' => 'Espanhol'][$registro->idioma] }}</td>
                <td>
                    <span class="glyphicon {{ $registro->publicado ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.novidades.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.novidades.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->appends($_GET)->links() !!}
    @endif

@endsection
