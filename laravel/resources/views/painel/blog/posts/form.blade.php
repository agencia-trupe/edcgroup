@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/blog/posts/'.$post->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_post', 'Texto Post (principal e obrigatório)') !!}
    {!! Form::textarea('texto_post', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politica']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_detalhe', 'Texto Detalhe - VER MAIS (opcional, só aparecerá se houverem mais de 1 imagem cadastrada)') !!}
    {!! Form::textarea('texto_detalhe', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politica']) !!}
</div>

<div class="form-group">
    {!! Form::label('hashtags', 'Hashtags (separar por espaço)') !!}
    {!! Form::textarea('hashtags', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
    <p class="exemplo-separacao" style="font-style: italic; color: red;"><strong>Exemplo:</strong> #hashtag1 #hashtag2 #hashtag3 (separar usando espaço)</p>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.posts.index') }}" class="btn btn-default btn-voltar">Voltar</a>