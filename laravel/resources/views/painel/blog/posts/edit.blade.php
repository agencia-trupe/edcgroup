@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Blog | Posts |</small> Editar Post</h2>
</legend>

{!! Form::model($post, [
'route' => ['painel.blog.posts.update', $post->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.blog.posts.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection