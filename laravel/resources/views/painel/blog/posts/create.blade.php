@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Blog | Posts |</small> Adicionar Post</h2>
</legend>

{!! Form::open(['route' => 'painel.blog.posts.store', 'files' => true]) !!}

@include('painel.blog.posts.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection