@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Blog | Posts
        <a href="{{ route('painel.blog.posts.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Post</a>
    </h2>
</legend>


@if(!count($posts))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="posts">
    <thead>
        <tr>
            <th>Data</th>
            <th>Capa</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($posts as $post)
        <tr class="tr-row" id="{{ $post->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($post->data)) }}</td>
            <td><img src="{{ asset('assets/img/blog/posts/'.$post->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>
                <a href="{{ route('painel.blog.posts.imagens.index', $post->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.blog.posts.destroy', $post->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.blog.posts.edit', $post->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection