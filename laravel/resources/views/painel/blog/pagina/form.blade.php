@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto Página Inicial') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'politica']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}