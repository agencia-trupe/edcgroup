@extends('painel.common.template')

@section('content')

<legend>
    <h2>Blog | Página</h2>
</legend>

{!! Form::model($blog, [
'route' => ['painel.blog.pagina.update', $blog->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.blog.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection