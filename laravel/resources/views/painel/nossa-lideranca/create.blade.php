@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Nossa Liderança /</small> Adicionar Pessoa</h2>
</legend>

{!! Form::open(['route' => 'painel.nossa-lideranca.store', 'files' => true]) !!}

@include('painel.nossa-lideranca.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection