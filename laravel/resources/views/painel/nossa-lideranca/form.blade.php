@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cargo_pt', 'Cargo PT') !!}
            {!! Form::text('cargo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cargo_en', 'Cargo EN') !!}
            {!! Form::text('cargo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('cargo_es', 'Cargo ES') !!}
            {!! Form::text('cargo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto ES') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoList']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/nossa-lideranca/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.nossa-lideranca.index') }}" class="btn btn-default btn-voltar">Voltar</a>