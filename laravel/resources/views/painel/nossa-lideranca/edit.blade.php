@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Nossa Liderança /</small> Editar Pessoa</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.nossa-lideranca.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.nossa-lideranca.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection