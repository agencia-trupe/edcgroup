@include('painel.common.flash')

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('title', 'Title Home') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Serviço Title
                </div>
                <div> 
                    {!! Form::text('servico_title', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Contato Title
                </div>
                <div> 
                    {!! Form::text('contato_title', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Quem Somos Title
                </div>
                <div> 
                    {!! Form::text('quemSomos_title', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Novidades Title
                </div>
                <div> 
                    {!! Form::text('novidades_title', null, ['class' => 'form-control']) !!}
                </div>
            </div>
           
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('description', 'Description Home') !!}
            {!! Form::text('description', null, ['class' => 'form-control']) !!}

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Serviço Description
                </div>
                <div> 
                    {!! Form::text('servico_description', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Contato Description
                </div>
                <div> 
                    {!! Form::text('contato_description', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Quem Somos Description
                </div>
                <div> 
                    {!! Form::text('quemSomos_description', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Novidades Description
                </div>
                <div> 
                    {!! Form::text('novidades_description', null, ['class' => 'form-control']) !!}
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('keywords', 'Keywords Home') !!}
            {!! Form::text('keywords', null, ['class' => 'form-control']) !!}

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Serviço KeyWords
                </div>
                <div> 
                    {!! Form::text('servico_keywords', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Contato KeyWords
                </div>
                <div> 
                    {!! Form::text('contato_keywords', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Quem Somos KeyWords
                </div>
                <div> 
                    {!! Form::text('quemSomos_keywords', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div style='justify-content:left;margin:5px 0px 0px 0px; width:100%;'>
                 <div style='background:#f8f086;padding:10px 7px 0px 7px;border-top-right-radius:15px;border-bottom-leftradius:7px;width:250px;'>
                    Novidades KeyWords
                </div>
                <div> 
                    {!! Form::text('novidades_keywords', null, ['class' => 'form-control']) !!}
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('analytics_ua', 'Código Analytics UA') !!}
            {!! Form::text('analytics_ua', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('analytics_gg', 'Código Analytics GG') !!}
            {!! Form::text('analytics_gg', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('codigo_gtm', 'Código Google Tag Manager (GTM)') !!}
            {!! Form::text('codigo_gtm', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pixel_facebook', 'Código Pixel do Facebook') !!}
            {!! Form::text('pixel_facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_de_compartilhamento', 'Imagem de compartilhamento') !!}
    @if($registro->imagem_de_compartilhamento)
    <img src="{{ url('assets/img/'.$registro->imagem_de_compartilhamento) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_de_compartilhamento', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}



