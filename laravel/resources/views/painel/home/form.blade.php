@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('banner', 'Banner') !!}
    @if($registro->banner)
    <img src="{{ url('assets/img/home/'.$registro->banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('banner', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('frase_banner_pt', 'Frase Banner PT') !!}
        {!! Form::text('frase_banner_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_banner_en', 'Frase Banner EN') !!}
        {!! Form::text('frase_banner_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_banner_es', 'Frase Banner ES') !!}
        {!! Form::text('frase_banner_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('video', 'Vídeo do YouTube') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="observacao" style="color: red; font-style: italic;">Incluir aqui somente a parte da url após o "v=". Exemplo: <span style="text-decoration: underline;">https://www.youtube.com/watch?v=<strong>M08wxK31Jzgs</strong></span></p>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('img_vagas', 'Imagem Vagas') !!}
    @if($registro->img_vagas)
    <img src="{{ url('assets/img/home/'.$registro->img_vagas) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('img_vagas', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('frase_vagas_pt', 'Frase Vagas PT') !!}
        {!! Form::text('frase_vagas_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_vagas_en', 'Frase Vagas EN') !!}
        {!! Form::text('frase_vagas_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('frase_vagas_es', 'Frase Vagas ES') !!}
        {!! Form::text('frase_vagas_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}