@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contratação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contratacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.contratacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
