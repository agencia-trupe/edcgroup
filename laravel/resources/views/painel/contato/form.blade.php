@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone', 'Telefone') !!}
            {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('whatsapp', 'Whatsapp') !!}
            {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('endereco_pt', 'Endereço PT') !!}
            {!! Form::textarea('endereco_pt', null, ['class' => 'form-control ckeditor', 'data-editor ' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('endereco_en', 'Endereço EN') !!}
            {!! Form::textarea('endereco_en', null, ['class' => 'form-control ckeditor', 'data-editor ' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('endereco_es', 'Endereço ES') !!}
            {!! Form::textarea('endereco_es', null, ['class' => 'form-control ckeditor', 'data-editor ' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Link Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', 'Link Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'Link LinkedIn') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link_vagas', 'Link Vagas') !!}
    {!! Form::text('link_vagas', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}