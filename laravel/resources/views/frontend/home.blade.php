@extends('frontend.common.template')

@section('content')

<div class="home">

    <div class="banner" style="background-image: url({{ asset('assets/img/home/'.$home->banner) }})">
        <h2 class="titulo-banner">{{ tobj($home, 'frase_banner') }}</h2>
        <div class="empresas-banner">
            @foreach($empresas as $empresa)
            <a href="{{ $empresa->link_website }}" target="_blank" class="link-empresa" id="{{ $empresa->slug_pt }}" title="{{ tobj($empresa, 'titulo') }}"></a>
            @endforeach
        </div>
    </div>

    <section class="empresas-servicos">
        @foreach($empresas as $empresa)
        <div class="empresa" id="{{ $empresa->slug_pt }}">



            @php
            $servicosEmpresa = $servicos->where('empresa_id', $empresa->id)->take(3);
            @endphp

            @foreach($servicosEmpresa as $servico)

            <p class="servico">{{ tobj($servico, 'titulo_servico') }} </p>

            <hr class="linha-divisao">
            @endforeach


            <div class="contato">
                <a href="{{ $empresa->link_contato }}" target="_blank" class="link-contato" title="{{ t('home.fale-com') }}">
                    {{ t('home.fale-com') }}
                </a>
            </div>
            <a href="{{ $empresa->link_website }}" target="_blank" class="link-website" title="{{ t('home.website') }}">{{ t('home.website') }}</a>
        </div>
        @endforeach
    </section>

    <section class="vagas">
        <div class="center">
            <img src="{{ asset('assets/img/home/'.$home->img_vagas) }}" class="img-vagas">
            <aside class="sobre-vagas">
                <h1 class="titulo-vagas">
                    {{ t('nav.vagas') }}
                    <img src="{{ asset('assets/img/layout/seta-vagas.svg') }}" class="img-seta" title="Vagas">
                </h1>
                <p class="frase-vagas">{{ tobj($home, 'frase_vagas') }}</p>
                <div>
                    <a href="{{ $home->link_vagas }}" class="link-vagas" alt="">{{ t('home.link-vagas') }}</a>
                </div>
            </aside>
        </div>
    </section>

    <section class="conheca center">
        <h1 class="titulo-conheca">{{ t('home.conheca') }}</h1>
        <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$home->video }}"></iframe>
        <div class="saiba-mais">

        @if(app()->getLocale() === 'en')
        <a href="{{ route('quem-somos') }}" class="link-saiba-mais">SEE MORE »</a>
        @endif

        @if(app()->getLocale() === 'pt')
        <a href="{{ route('quem-somos') }}" class="link-saiba-mais">SAIBA MAIS »</a>
        @endif

        @if(app()->getLocale() === 'es')
        <a href="{{ route('quem-somos') }}" class="link-saiba-mais">SABER MAS »</a>
        @endif

           
        
        </div>
    </section>

    @if(count($novidades) > 0)
    <section class="novidades">
        <h1 class="titulo-novidades">{{ t('home.novidades') }}</h1>
        <div class="itens-novidades">
            @foreach($novidades as $novidade)
            <a href="{{ route('novidades.show', [$novidade->categoria_slug, $novidade->slug]) }}" class="link-novidade">
                <div class="categoria">{{ tobj($novidade, 'titulo') }}</div>
                <h4 class="titulo">{{ $novidade->titulo }}</h4>
                <img src="{{ asset('assets/img/novidades/'.$novidade->capa) }}" class="img-novidade">
            </a>
            @endforeach
        </div>
    </section>
    @endif

    <section class="depoimentos">
        <div class="center">
            <h2>
                <span>
                    {{ t('home.opiniao') }}<br>
                    {{ t('home.edc-clientes') }}
                </span>
            </h2>

            <div class="depoimentos-wrapper">
                @foreach($depoimentos as $d)
                <div>
                    <p>"{!! tobj($d, 'depoimento') !!}"</p>
                    <p class="nome">{{ $d->nome }}</p>
                </div>
                @endforeach
            </div>
    </section>
</div>

</div>

@endsection