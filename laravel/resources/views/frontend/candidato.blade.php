@extends('frontend.common.template')

@section('content')

<div class="area-do-candidato">
    <div class="center">
        <div class="left">

            @if(app()->getLocale() === 'en')
             <h1>CANDIDATE AREA</h1>
            @endif

            @if(app()->getLocale() === 'pt')
             <h1>{{ t('candidato.titulo') }}</h1>
            @endif

            @if(app()->getLocale() === 'es')
             <h1>ÁREA CANDIDATA</h1>
            @endif
           
            <div class="texto-candidato">

                @if(app()->getLocale() === 'en')
                <p>Hi,</p>
                <p>This area is dedicated to you!</p>
                <p>Here you can ﬁnd out about all the job opportunities that EDC oﬀers.</p>
                @endif

                @if(app()->getLocale() === 'pt')
                <p>{!! t('candidato.ola') !!}</p>
                <p>{!! t('candidato.area-dedicada') !!}</p>
                <p>{!! t('candidato.oportunidades') !!}</p>
                @endif

                @if(app()->getLocale() === 'es')
                <p>Hola,</p>
                <p>¡Esta área está dedicada a ti!</p>
                <p>Aquí podrás conocer todas las oportunidades laborales que ofrece la EDC.</p>
                @endif

               
            </div>


            <div class="links">
                @if($contato->link_vagas)
                <a href="{{ Tools::parseLink($contato->link_vagas) }}" target="_blank">
                    <img src="{{ asset('assets/img/layout/icone-vagas.svg') }}" alt="{{ t('candidato.vagas') }}">

              

                @if(app()->getLocale() === 'pt')
                <h2>{{ t('candidato.vagas') }}</h2>
                <p>{{ t('candidato.vagas-texto') }}</p>
                @endif

                @if(app()->getLocale() === 'en')
                <h2>JOB OPPORTUNITIES</h2>
                <p>Come work with us.Register your active CV and check out our vacancies.</p>
                @endif

                @if(app()->getLocale() === 'es')
                <h2>VACANTES</h2>
                <p>Ven a trabajar con nosotros.Registra tu CV activo y consulta nuestras vacantes.</p>
                @endif

                   
                </a>
                @endif

                <a href="{{ route('trainees') }}">
                    <img src="{{ asset('assets/img/layout/icone-trainees.svg') }}" alt="{{ t('candidato.trainees') }}">

                @if(app()->getLocale() === 'pt')
                <h2>{{ t('candidato.trainees') }}</h2>
                <p>{{ t('candidato.trainees-texto') }}</p>
                @endif

                @if(app()->getLocale() === 'en')
                <h2>TRAINEE PROGRAM</h2>
                <p>Check out our 2021 Trainee Program and stay tuned for upcoming editions of the Program.</p>
                @endif

                @if(app()->getLocale() === 'es')
                <h2>PROGRAMA DE ENTRENAMIENTO</h2>
                <p>Consulte nuestro Programa de aprendices 2021 y esté atento a las próximas ediciones del Programa.</p>
                @endif

                  
                </a>
            </div>
        </div>

        <div class="right">
            @foreach($depoimentos as $depoimento)
            <div class="depoimento">
                <p class="texto">"{!! tobj($depoimento, 'depoimento') !!}"</p>
                <p class="nome">{{ $depoimento->nome }}</p>
            </div>
            @endforeach
            <a href=# id="next"><img src="{{ asset('assets/img/layout/simbolo-mais-losangoverde.svg') }}" alt=""></a>
        </div>

    </div>

    <iframe class="video" src="{{ 'https://www.youtube.com/embed/ZmI3r3PHEwM' }}"></iframe>
</div>

@endsection