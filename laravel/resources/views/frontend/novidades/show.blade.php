@extends('frontend.common.template')

@section('content')

<div class="novidades">
    <div class="title">
        <div class="center">
            <div class="topo">
                <a href="{{ route('novidades') }}" class="titulo-novidades">{{ t('nav.novidades') }}</a>
                <form action="{{ route('novidades.busca') }}" method="GET" class="busca">
                    <input type="text" name="q" placeholder="{{ t('novidades.busca') }}" value="{{ Tools::routeIs('novidades.busca') ? request()->get('q') : '' }}" required>
                </form>
            </div>
        </div>
    </div>

    <div class="container center">
        @include('frontend.novidades._aside')

        <main>
            <div class="novidades-show">
                <div class="cover">
                    <img src="{{ asset('assets/img/novidades/'.$novidade->capa) }}" alt="">
                    <div class="overlay">
                        <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <h1>{{ $novidade->titulo }}</h1>
                    </div>
                </div>

                <div class="texto">
                    {!! $novidade->texto !!}
                </div>
            </div>
        </main>
    </div>

    @if(count($relacionados))
    <div class="relacionados center">
        <h4>{{ t('novidades.relacionados') }}</h4>

        <div class="relacionados-lista">
            @foreach($relacionados as $novidade)
            <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}" class="thumb-lista">
                <div class="imagem">
                    <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                </div>
                <div class="info">
                    <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
                    <p class="titulo">{{ $novidade->titulo }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>
    @endif
</div>

@endsection