@foreach($novidades as $novidade)
    <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}" class="thumb-simple">
        <div class="info">
            <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
            <p class="titulo">{{ $novidade->titulo }}</p>
        </div>
    </a>
@endforeach
