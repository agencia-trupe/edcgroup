@extends('frontend.common.template')

@section('content')

<div class="novidades">
    <div class="title">
        <div class="center">
            <div class="topo">
                <a href="{{ route('novidades') }}" class="titulo-novidades">{{ t('nav.novidades') }}</a>
                <form action="{{ route('novidades.busca') }}" method="GET" class="busca">
                    <input type="text" name="q" placeholder="{{ t('novidades.busca') }}" value="{{ Tools::routeIs('novidades.busca') ? request()->get('q') : '' }}" required>
                </form>
            </div>
        </div>
    </div>

    <div class="container center">
        @include('frontend.novidades._aside')

        <main>
            @if(!count($novidades))
            <div class="nenhum">{{ t('novidades.nenhum') }}</div>
            @else
            <div class="novidades-lista">
                @foreach($novidades as $novidade)
                <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}" class="thumb-lista">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                    </div>
                    <div class="info">
                        <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <p class="titulo">{{ $novidade->titulo }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            @endif

            {!! $novidades->appends($_GET)->render() !!}
        </main>
    </div>
</div>

@endsection