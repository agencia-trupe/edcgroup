@extends('frontend.common.novidades_template')

@section('content')

<div class="novidades">
    <div class="title">
        <div class="center">
            <div class="topo">
                <a href="{{ route('novidades') }}" class="titulo-novidades">{{ t('nav.novidades') }}</a>
                <form action="{{ route('novidades.busca') }}" method="GET" class="busca">
                    <input type="text" name="q" placeholder="{{ t('novidades.busca') }}" value="{{ Tools::routeIs('novidades.busca') ? request()->get('q') : '' }}" required>
                </form>
            </div>

            <div class="destaques">
                @foreach($novidades->slice(0, 2) as $novidade)
                <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}">
                    <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                    <div class="overlay">
                        <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <p class="titulo">{{ $novidade->titulo }}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container center">
        @include('frontend.novidades._aside')

        <main>
            @if(!count($novidades))
            <div class="nenhum">{{ t('novidades.nenhum') }}</div>
            @else
            <div class="novidades-index">
                @foreach($novidades->slice(2) as $index => $novidade)
                <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}" class="{{ $index < 5 ? 'thumb-image' : 'thumb-simple' }}">
                    <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                    <div class="info">
                        <span class="categoria">{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <p class="titulo">{{ $novidade->titulo }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            @endif

            @if($novidades->hasMorePages())
            <button class="novidades-ver-mais" type="button" data-next="{{ route('novidades', ['page' => 2]) }}">
                {{ t('novidades.ver-mais') }}
            </button>
            @endif
        </main>
    </div>
</div>

@endsection