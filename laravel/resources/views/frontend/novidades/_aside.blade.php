<aside>
    @if(count($categorias))
    <div class="categorias">
        @foreach($categorias as $c)
            <a href="{{ route('novidades.categoria', $c->slug) }}" @if(isset($categoria) && $categoria->id === $c->id) class="active" @endif>
                {{ tobj($c, 'titulo') }}
            </a>
        @endforeach
    </div>
    @endif
</aside>
