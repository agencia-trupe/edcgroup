@extends('frontend.common.template')

@section('content')

<div class="trainees">
    <div class="center">
        <h1>{{ t('trainees.titulo') }}</h1>
    </div>

    <div class="banner">
        <img src="{{ asset('assets/img/layout/faixa-titulo.png') }}" alt="" class="img-banner">
        <div class="dados-banner">
            <p class="titulo-banner">{{ t('trainees.banner.titulo') }}</p>
            <p class="fase-banner">{{ t('trainees.banner.fase') }}</p>
            <a href="javascript: $('html, body').animate({ scrollTop: $('#sectionLinkVagas').offset().top }, 600);" class="inscricao-banner" style="display: none;">
                <p class="link-banner">{{ t('trainees.banner.link') }}</p>
            </a>
        </div>
    </div>

    <div class="quem-somos">
        <div class="center">
            <h1>{{ t('trainees.edc.titulo') }}</h1>
            <div class="sobre-edc">
                <div class="left">
                    <img src="{{ asset('assets/img/layout/grafico-EDC-eng-desenv-consultoria.svg') }}" alt="" class="img-edc">
                    <p class="local">{{ t('trainees.edc.local') }}</p>
                    <p class="empresa">{{ t('trainees.edc.funcionarios') }}</p>
                </div>
                <div class="right">
                    <div class="texto-edc">{!! t('trainees.edc.texto') !!}</div>
                    <h1 class="titulo-pilares">{{ t('trainees.edc.pilares') }}</h1>
                    <p class="pilares">» {{ t('trainees.edc.pilar1') }}</p>
                    <p class="pilares">» {{ t('trainees.edc.pilar2') }}</p>
                    <p class="pilares">» {{ t('trainees.edc.pilar3') }}</p>
                    <p class="pilares">» {{ t('trainees.edc.pilar4') }}</p>
                    <p class="pilares">» {{ t('trainees.edc.pilar5') }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="programa-qualificacoes">
        <div class="center">
            <div class="programa">
                <h1 class="titulo-programa">{{ t('trainees.programa.titulo') }}</h1>
                <p class="texto-programa">{{ t('trainees.programa.texto1') }} <strong>{{ t('trainees.programa.destaque1') }}</strong> {{ t('trainees.programa.texto2') }} <strong>{{ t('trainees.programa.destaque2') }}</strong></p>
            </div>
            <img src="{{ asset('assets/img/layout/img-ilustra-programa.png') }}" alt="" class="img-programa">
            <div class="qualificacoes">
                <h1 class="titulo-qualif">{{ t('trainees.programa.qualificacoes') }}</h1>
                <p class="item-qualif">{{ t('trainees.programa.necessarias') }}</p>
                <p class="item-qualif"><span>•</span>{{ t('trainees.programa.qualificacao1') }}</p>
                <p class="item-qualif"><span>•</span>{{ t('trainees.programa.qualificacao2') }}</p>
                <p class="item-qualif"><span>•</span>{{ t('trainees.programa.qualificacao3') }}</p>
                <p class="item-qualif"><span>•</span>{{ t('trainees.programa.qualificacao4') }}</p>
                <p class="obs-qualif">{{ t('trainees.programa.time-edc') }}</p>
            </div>
        </div>
    </div>

    <div class="vagas-requisitos">
        <div class="center">
            <h1>{{ t('trainees.requisitos.titulo') }}</h1>
            <p class="frase">{{ t('trainees.requisitos.frase') }}</p>
            <div class="vaga1">
                <div class="left">
                    <div class="area">{{ t('trainees.requisitos.vaga1.area') }}</div>
                    <div class="setores">
                        <p class="titulo-setor">{{ t('trainees.requisitos.vaga1.setor') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga1.set1') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga1.set2') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga1.set3') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga1.set4') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga1.set5') }}</p>
                    </div>
                    <div class="experiencia">
                        <p class="titulo-exp">{{ t('trainees.requisitos.vaga1.experiencia') }}</p>
                        <p class="texto-exp">{{ t('trainees.requisitos.vaga1.exp-texto') }}</p>
                    </div>
                </div>
                <div class="right">
                    <div class="requisitos">
                        <p class="titulo-req">{{ t('trainees.requisitos.vaga1.habilidades') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab1') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab2') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab3') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab4') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab5') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab6') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab7') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga1.hab8') }}</p>
                    </div>
                    <div class="atividades">
                        <p class="titulo-ativ">{{ t('trainees.requisitos.vaga1.atividades') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ1') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ2') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ3') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ4') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ5') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ6') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga1.ativ7') }}</p>
                    </div>
                    <a href="javascript: $('html, body').animate({ scrollTop: $('#sectionLinkVagas').offset().top }, 600);" class="inscricao" style="display: none;">
                        <p class="link-inscricao">{{ t('trainees.requisitos.vaga1.inscreva-se') }}</p>
                    </a>
                </div>
            </div>
            <div class="vaga2">
                <div class="left">
                    <div class="area">{{ t('trainees.requisitos.vaga2.area') }}</div>
                    <div class="setores">
                        <p class="titulo-setor">{{ t('trainees.requisitos.vaga2.setor') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga2.set1') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga2.set2') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga2.set3') }}</p>
                        <p class="setor">{{ t('trainees.requisitos.vaga2.set4') }}</p>
                    </div>
                    <div class="experiencia">
                        <p class="titulo-exp">{{ t('trainees.requisitos.vaga2.experiencia') }}</p>
                        <p class="texto-exp">{{ t('trainees.requisitos.vaga2.exp-texto') }}</p>
                    </div>
                </div>
                <div class="right">
                    <div class="requisitos">
                        <p class="titulo-req">{{ t('trainees.requisitos.vaga2.habilidades') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab1') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab2') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab3') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab4') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab5') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab6') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab7') }}</p>
                        <p class="requisito">• {{ t('trainees.requisitos.vaga2.hab8') }}</p>
                    </div>
                    <div class="atividades">
                        <p class="titulo-ativ">{{ t('trainees.requisitos.vaga2.atividades') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ1') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ2') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ3') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ4') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ5') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ6') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ7') }}</p>
                        <p class="atividade">• {{ t('trainees.requisitos.vaga2.ativ8') }}</p>
                    </div>
                    <a href="javascript: $('html, body').animate({ scrollTop: $('#sectionLinkVagas').offset().top }, 600);" class="inscricao" style="display: none;">
                        <p class="link-inscricao">{{ t('trainees.requisitos.vaga2.inscreva-se') }}</p>
                    </a>
                </div>
            </div>
            <div class="caracteristicas">
                <div class="left">
                    <div class="titulo-carac">{{ t('trainees.requisitos.caracteristicas.titulo') }}</div>
                    <div class="tipo">
                        <p class="titulo-tipo">{{ t('trainees.requisitos.caracteristicas.tipo') }}</p>
                        <p class="texto-tipo">{{ t('trainees.requisitos.caracteristicas.tipo-texto') }}</p>
                    </div>
                    <div class="funcoes">
                        <p class="titulo-funcoes">{{ t('trainees.requisitos.caracteristicas.funcoes') }}</p>
                        <p class="texto-funcoes">{{ t('trainees.requisitos.caracteristicas.funcoes-texto') }}</p>
                    </div>
                    <div class="nivel">
                        <p class="titulo-nivel">{{ t('trainees.requisitos.caracteristicas.nivel') }}</p>
                        <p class="texto-nivel">{{ t('trainees.requisitos.caracteristicas.nivel-texto') }}</p>
                    </div>
                    <div class="local">
                        <p class="titulo-local">{{ t('trainees.requisitos.caracteristicas.local') }}</p>
                        <p class="texto-local">{{ t('trainees.requisitos.caracteristicas.local-texto') }}</p>
                    </div>
                </div>
                <div class="right">
                    <div class="beneficios">
                        <p class="titulo-benef">{{ t('trainees.requisitos.caracteristicas.beneficios') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef1') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef2') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef3') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef4') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef5') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef6') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef7') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef8') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef9') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef10') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef11') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef12') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef13') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef14') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef15') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef16') }}</p>
                        <p class="beneficio">• {{ t('trainees.requisitos.caracteristicas.benef17') }}</p>
                    </div>
                </div>
            </div>

            <a href="{{ route('trainees.pdf') }}" class="download" target="_blank">
                <p class="link-download">{{ t('trainees.requisitos.download') }}</p>
            </a>
        </div>
    </div>

    <div class="inscricoes" id="sectionLinkVagas" style="display: none;">
        <div class="center">
            <h1 class="titulo-inscricoes">{{ t('trainees.inscricoes') }}</h1>
            <div class="opcoes-inscricoes">
                <a href="https://www.portalsinergyrh.com.br/Portal/MeuPortal/VisualizarVaga?idVaga=733559" target="_blank" class="opcao">{{ t('trainees.setor-rh') }}</a>
                <a href="https://www.portalsinergyrh.com.br/Portal/MeuPortal/VisualizarVaga?idVaga=733565" target="_blank" class="opcao">{{ t('trainees.setor-eng') }}</a>
            </div>
        </div>
    </div>

    <div class="fases-do-processo">
        <div class="center">
            <h1 class="titulo-fases">{{ t('trainees.processo.titulo') }}</h1>
            <p class="obs-fases">{{ t('trainees.processo.frase') }}</p>
            <div class="etapas">
                <div class="etapa0">
                    <img src="{{ asset('assets/img/layout/ico-0etapa.svg') }}" alt="" class="img-etapa">
                    <p class="frase-etapa">{{ t('trainees.processo.etapa0') }}</p>
                    <p class="data-etapa">{{ t('trainees.processo.etapa0-data') }}</p>
                </div>
                <div class="etapa1">
                    <div class="active"></div>
                    <img src="{{ asset('assets/img/layout/ico-1etapa.svg') }}" alt="" class="img-etapa">
                    <p class="frase-etapa">{{ t('trainees.processo.etapa1') }}</p>
                    <!-- <p class="data-etapa">{{ t('trainees.processo.etapa1-data') }}</p> -->
                    <div class="etapa-atual">
                        <img src="{{ asset('assets/img/layout/setinha-fase.svg') }}" alt="" class="img-fase-atual">
                        <p class="fase-atual">{{ t('trainees.processo.fase-atual') }}</p>
                    </div>
                </div>
                <div class="etapa2">
                    <img src="{{ asset('assets/img/layout/ico-2etapa.svg') }}" alt="" class="img-etapa">
                    <p class="frase-etapa">{{ t('trainees.processo.etapa2') }}</p>
                    <!-- <p class="data-etapa">{{ t('trainees.processo.etapa2-data') }}</p> -->
                </div>
                <div class="etapa3">
                    <img src="{{ asset('assets/img/layout/ico-3etapa.svg') }}" alt="" class="img-etapa">
                    <p class="frase-etapa">{{ t('trainees.processo.etapa3') }}</p>
                    <!-- <p class="data-etapa">{{ t('trainees.processo.etapa3-data') }}</p> -->
                </div>
                <div class="etapa4">
                    <img src="{{ asset('assets/img/layout/ico-4etapa.svg') }}" alt="" class="img-etapa">
                    <p class="frase-etapa">{{ t('trainees.processo.etapa4') }}</p>
                    <!-- <p class="data-etapa">{{ t('trainees.processo.etapa4-data') }}</p> -->
                </div>
            </div>
        </div>
    </div>

    <div class="hashtag">{{ t('trainees.hashtag') }}</div>
</div>

@endsection