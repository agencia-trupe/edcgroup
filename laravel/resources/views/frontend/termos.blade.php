@extends('frontend.common.template')

@section('content')

    <div class="pagina-texto">
        <div class="center">
            <h1>{{ t('nav.termos') }}</h1>
            {!! tobj($termos, 'texto') !!}
        </div>
    </div>

@endsection
