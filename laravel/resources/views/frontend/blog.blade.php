@extends('frontend.common.template')

@section('content')

<div class="blog">
    <div class="dados-blog">
        <div class="center">
            <h1>BLOG · DICAS DA GRAZI</h1>
            <div class="dados">
                <div class="img">
                    <img src="{{ asset('assets/img/layout/avatar-grazi-blog.png') }}" alt="" class="img-grazi">
                </div>
                <div class="dicas">
                    {!! $blog->texto !!}
                    <img src="{{ asset('assets/img/layout/marca-dicas-da-grazi.png') }}" alt="" class="img-dicas">
                    <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram">Siga: <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-instagram"><span class="nome">@edcgrouprh</span></a>
                </div>
            </div>
        </div>
    </div>

    <div class="posts">
        <div class="center">
            @foreach($posts as $post)
            <div class="post">
                <img src="{{ asset('assets/img/blog/posts/'.$post->capa) }}" alt="" class="img-capa-post">
                <p class="data">{{ strftime("%d %b %Y", strtotime($post->data)) }}</p>
                <div class="texto-post">{!! $post->texto_post !!}</div>
                <div class="hashtags">
                    @if($post->hashtags)
                    @php $hashtags = explode(" ", $post->hashtags); @endphp
                    @foreach($hashtags as $hashtag)
                    @php $linkHashtag = str_replace("#", "", $hashtag); @endphp
                    <a href="https://www.instagram.com/explore/tags/{{$linkHashtag}}/" target="_blank" class="link-hashtag">{{ $hashtag }}</a>
                    @endforeach
                    @endif
                </div>
                @if(count($imagens->where('post_id', $post->id)) > 0)
                <a href="{{ route('post.detalhes', $post->id) }}" class="btn-ver-mais">VER MAIS +</a>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection