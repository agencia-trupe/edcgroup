<footer>
    <div class="newsletter">
        <form action="{{ route('newsletter.post') }}" method="post" class="form-newsletter">
            {!! csrf_field() !!}
            <div class="row">
                <h4 class="titulo">{{ t('nav.titulo-newsletter') }}</h4>
                <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <button type="submit" class="btn-newsletter">»</button>
            </div>

            @if($errors->any())
            <div class="flash flash-error">{{ t('contato.newsletter-erro') }}</div>
            @endif
            @if(session('enviado'))
            <div class="flash flash-success">{{ t('contato.newsletter-ok') }}</div>
            @endif
        </form>
    </div>

    <hr class="linha-footer">

    <div class="selos-footer">
        <div class="center">
            <div class="selo-iso">
                <img src="{{ asset('assets/img/layout/selo-qualidade-iso9001.svg') }}" alt="" class="img-selo">
                <img src="{{ asset('assets/img/layout/selo-qualidade-iso14001.svg') }}" alt="SLAC" class="img-selo">
                <div class="textos">
                    <p>{{ t('footer.selo-iso-1') }} <strong>{{ t('footer.selo-iso-bold') }}</strong></p>
                    <p>{{ t('footer.selo-iso-2') }}</p>
                    <p>{{ t('footer.selo-iso-3') }}</p>
                </div>
            </div>
            {{-- <div class="selo-slac">
                <img src="{{ asset('assets/img/layout/selo-SLAC-rodape.png') }}" alt="">
                <div class="textos">
                    <p>{{ t('footer.selo-slac-1') }}</p>
                    <p><strong>{{ t('footer.selo-slac-bold') }}</strong> {{ t('footer.selo-slac-2') }}</p>
                    <p>{{ t('footer.selo-slac-3') }}</p>
                </div>
            </div> --}}
        </div>
    </div>

    <hr class="linha-footer">

    <div class="center">
        <article class="vagas-areas">
            @if($contato->link_vagas)
            <a href="{{ Tools::parseLink($contato->link_vagas)}}" target="_blank" class="link-vagas">
                {{ t('nav.vagas') }}
            </a>
            @endif
            <a href="{{ route('candidato') }}" class="link-area" target="_blank">
                {{ t('nav.area-do-candidato') }}
            </a>
            <a href="{{ route('consultor') }}" class="link-area" target="_blank">
                {{ t('nav.area-do-consultor') }}
            </a>
        </article>

        {{-- <article class="marcas">
            <a href="{{ $edcServicos->link_website }}" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-servicos-branco.svg') }}" alt="{{ tobj($edcServicos, 'titulo') }}" class="img-logo">
            </a>
            <a href="{{ $edcEngenharia->link_website }}" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-engenharia-branco.svg') }}" alt="{{ tobj($edcEngenharia, 'titulo') }}" class="img-logo">
            </a>
            <a href="{{ $edcUni->link_website }}" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-uni-branco.svg') }}" alt="{{ tobj($edcUni, 'titulo') }}" class="img-logo">
            </a>
        </article> --}}

        <article class="marcas">
            <a href="https://edcservicos.com.br" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-servicos-branco.svg') }}" alt="edcservicos.com.br" class="img-logo">
            </a>
            <a href="https://edcengenharia.com.br" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-engenharia-branco.svg') }}" alt="edcengenharia.com.br" class="img-logo">
            </a>
            <a href="https://edcsmart.com.br" class="link-empresa">
                <img src="{{ asset('assets/img/layout/marca-edc-smart.png') }}" alt="edcsmart.com.br" class="img-logo">
            </a>
        </article>

        <article class="contatos">
            <div class="redes-sociais">
                <a href="{{ route('home') }}" class="link-home">
                    <img src="{{ asset('assets/img/layout/marca-edc-rodape.svg') }}" alt="{{ tobj($config, 'title') }}" class="img-logo">
                </a>
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook" title="Facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin" title="LinkedIn"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt=""></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram"><img src="{{ asset('assets/img/layout/ico-instagram-topo.svg') }}" alt=""></a>
            </div>
            <div class="informacoes">
                @php
                $telefone = str_replace(" ", "", $contato->telefone);
                $whatsapp = str_replace(" ", "", $contato->whatsapp);
                @endphp
                <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-telefone" target="_blank">{{ $contato->whatsapp }}</a>
                <a href="tel:+55{{ $contato->telefone }}" class="link-telefone">{{ $contato->telefone }}</a>
                <a href="mailto:{{ $contato->email }}" class="link-email">{{ $contato->email }}</a>
            </div>
            <p class="endereco">{!! tobj($contato, 'endereco') !!}</p>
        </article>

        <article class="nav-footer">
            <a href="{{ route('empresas') }}" class="link-footer @if(Tools::routeIs('empresas')) active @endif">
                <span>•</span>{{ t('nav.empresas') }}
            </a>
            <a href="{{ route('quem-somos') }}" class="link-footer @if(Tools::routeIs('quem-somos')) active @endif">
                <span>•</span>{{ t('nav.quem-somos') }}
            </a>
            @if(app()->getLocale() === 'pt')
            <a href="{{ route('novidades') }}" class="link-footer @if(Tools::routeIs('novidades')) active @endif">
                <span>•</span>{{ t('nav.novidades') }}
            </a>
            <a href="{{ route('blog') }}" class="link-footer @if(Tools::routeIs('blog')) active @endif">
                <span>•</span>{{ t('nav.blog') }}
            </a>
            @endif
            <a href="{{ route('contato') }}" class="link-footer @if(Tools::routeIs('contato')) active @endif">
                <span>•</span>{{ t('nav.contato') }}
            </a>

            <a href="{{ route('termos-de-uso') }}" class="link-termos @if(Tools::routeIs('termos-de-uso')) active @endif" target="_blank">
                » {{ t('nav.termos') }}
            </a>
            <a href="{{ route('politica-de-privacidade') }}" class="link-politica @if(Tools::routeIs('politica-de-privacidade')) active @endif" target="_blank">
                » {{ t('nav.politica') }}}
            </a>
        </article>
    </div>
    <article class="copyright">
        <p class="direitos"> © {{ date('Y') }} {{ config('app.name') }} - {{ t('footer.copyright') }}</p>
        <span>|</span>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">{{ t('footer.criacao-sites') }}</a>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
    </article>
</footer>