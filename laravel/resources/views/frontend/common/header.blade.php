<section class="nav-topo">
    <div class="center">
        @if($contato->link_vagas)
        <a href="{{ Tools::parseLink($contato->link_vagas)}}" target="_blank" class="link-vagas">
            {{ t('nav.vagas') }}
            <span>| {{ t('nav.vagas-texto') }}</span>
        </a>
        @endif

        <article class="right">
            <a href="{{ route('candidato') }}" class="link-topo @if(Tools::routeIs('candidato*')) active @endif" target="_blank">
                {{ t('nav.area-do-candidato') }}
            </a>
            <a href="{{ route('consultor') }}" class="link-topo @if(Tools::routeIs('consultor*')) active @endif" target="_blank">
                {{ t('nav.area-do-consultor') }}
            </a>

            <select name="lang">
                @foreach(['pt', 'en', 'es'] as $lang)
                <option value="{{ $lang }}" @if(app()->getLocale() === $lang) selected @endif data-route="{{ route('lang', $lang) }}">
                    {{ strtoupper($lang) }}
                </option>
                @endforeach
            </select>

            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook" title="Facebook">
                    <!-- <img src="{{ asset('assets/img/layout/ico-facebook-cinza.svg') }}" alt=""> -->

                </a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin" title="LinkedIn">
                    <!-- <img src="{{ asset('assets/img/layout/ico-linkedin-cinza.svg') }}" alt=""> -->
                </a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram">
                    <!-- <img src="{{ asset('assets/img/layout/ico-instagram-cinza.svg') }}" alt=""> -->
                </a>
            </div>
        </article>
    </div>
</section>

<header>
    <div class="center">

       
       

        @if(app()->getLocale() === 'en')
        <img src="https://edcgroup.com.br/marca-edc-slogan-ENG.svg" class='img-logo' alt='' />
        @endif

        @if(app()->getLocale() === 'pt')
        <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
        @endif

        @if(app()->getLocale() === 'es')
        <img src="https://edcgroup.com.br/marca-edc-slogan-ESP.svg" class='img-logo' alt='' />
        @endif

        

        <nav id="nav-desktop">
            @include('frontend.common.nav')
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

    </div>

    <nav id="nav-mobile">
        @include('frontend.common.nav')
    </nav>
</header>