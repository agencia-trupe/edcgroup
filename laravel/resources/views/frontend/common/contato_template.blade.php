<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->contato_description }}">
    <meta name="keywords" content="{{ $config->contato_keywords }}">

    <meta property="og:title" content="{{ $config->contato_title }}">
    <meta property="og:description" content="{{ $config->contato_description }}">
    <meta property="og:site_name" content="{{ $config->contato_title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    @if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
    @endif

    <title>{{ $config->contato_title }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/vendor.main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fancybox/source/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/floating-whatsapp/floating-wpp.min.css') }}">

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <style>

.contato-interesse{
    margin:5px 0px 5px 0px;
    font-family: Open Sans, Helvetica, Arial, sans-serif;
    padding:0px 0px 0px 4px;
    font-size:14px;
    border:1px solid #fff;
    width:100%;
    background:#fff;
    color: rgb(96, 96, 98);
}

        @media only screen and (max-width:767px){
        
        .img-footer-servicos, .img-footer-engenharia, .img-footer-uni{
        width:100%;
        }
        
        .contato-interesse{
        margin:7px 0px 7px 0px;    
        font-size:13px;
        }
        
        .img-logo{
        width: 80%;
        }  
        .b-box div{
        width:100%;
        text-align: center;
        }
        .b-box img{
        float:none; 
        padding:0px 0px 0px 0px; 
        width:50%;
        }
        }
        </style>


    @if($config->codigo_gtm)
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '{{ $config->codigo_gtm }}');
    </script>
    @endif

    @if($config->pixel_facebook)
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '{{ $config->pixel_facebook }}');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id={{ $config->pixel_facebook }}&ev=PageView
&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    @endif


</head>

<body>
    @if($config->codigo_gtm)
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ $config->codigo_gtm }}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif

    @include('frontend.common.header')
    @yield('content')
    @include('frontend.common.footer')

    <div id="chatWhatsapp"></div>

    <!-- AVISO DE COOKIES -->
    @if(!isset($verificacao))
    <div class="aviso-cookies">
        <p class="frase-cookies">{{ t('cookies.texto1') }}<a href="{{ route('politica-de-privacidade') }}" target="_blank" class="link-politica">{{ t('cookies.politica') }}</a>{{ t('cookies.texto2') }}</p>
        <button class="aceitar-cookies">{{ t('cookies.aceitar') }}</button>
    </div>
    @endif

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="{{ asset(' + "assets/vendor/jquery/dist/jquery.min.js" + ') }}"><\/script>')
    </script>
    <script src="{{ asset('assets/js/vendor.main.js') }}"></script>
    <script src="{{ asset('assets/vendor/fancybox/source/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-cycle2/build/jquery.cycle2.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/masonry-layout/dist/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/floating-whatsapp/floating-wpp.min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script>
        var routeHome = '{{ route("home") }}' + "/";
        var whatsappNumero = '{{ str_replace(" ", "", $contato->whatsapp) }}';
        var imgMarcaWhatsapp = '{{ asset("assets/img/layout/marca-edc.svg") }}';
    </script>

    @if($config->analytics_ua)
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', '{{ $config->analytics_ua }}', 'auto');
        ga('send', 'pageview');
    </script>
    @endif

    @if($config->analytics_gg)
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ $config->analytics_gg }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '{{ $config->analytics_gg }}');
    </script>

    @endif
</body>

</html>