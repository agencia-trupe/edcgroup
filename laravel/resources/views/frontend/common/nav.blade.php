<a href="{{ route('empresas') }}" @if(Tools::routeIs('empresas')) class="active" @endif>
    {{ t('nav.empresas') }}
</a>
<a href="{{ route('quem-somos') }}" @if(Tools::routeIs('quem-somos')) class="active" @endif>
    {{ t('nav.quem-somos') }}
</a>
@if(app()->getLocale() === 'pt')
<a href="{{ route('novidades') }}" @if(Tools::routeIs('novidades*')) class="active" @endif>
    {{ t('nav.novidades') }}
</a>
{{-- <a href="{{ route('blog') }}" @if(Tools::routeIs('blog*')) class="active" @endif>
    {{ t('nav.blog') }}
</a> --}}
@endif
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
    {{ t('nav.contato') }}
</a>
