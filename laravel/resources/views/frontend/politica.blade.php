@extends('frontend.common.template')

@section('content')

    <div class="pagina-texto">
        <div class="center">
            <h1>{{ t('nav.politica') }}</h1>
            {!! tobj($politica, 'texto') !!}
        </div>
    </div>

@endsection
