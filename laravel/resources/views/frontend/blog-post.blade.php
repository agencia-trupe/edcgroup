@extends('frontend.common.template')

@section('content')

<div class="blog-post">
    <div class="dados-blog">
        <div class="center">
            <div class="textos">
                <h1>BLOG · DICAS DA GRAZI</h1>
                <a href="{{ $blog->instagram }}" target="_blank" class="instagram" title="Instagram">Siga: <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-instagram"><span class="nome">@edcgrouprh</span></a>
            </div>
            <div class="imgs">
                <img src="{{ asset('assets/img/layout/avatar-grazi-blog.png') }}" alt="" class="img-grazi">
                <img src="{{ asset('assets/img/layout/marca-dicas-da-grazi.png') }}" alt="" class="img-dicas">
            </div>
        </div>
    </div>

    <div class="dados-post">
        <div class="center">
            <div class="textos-hashtags">
                <div class="texto-post">{!! $post->texto_post !!}</div>
                @if($post->hashtags)
                <div class="hashtags">
                    @php $hashtags = explode(" ", $post->hashtags); @endphp
                    @foreach($hashtags as $hashtag)
                    @php $linkHashtag = str_replace("#", "", $hashtag); @endphp
                    <a href="https://www.instagram.com/explore/tags/{{$linkHashtag}}/" target="_blank" class="link-hashtag">{{ $hashtag }}</a>
                    @endforeach
                </div>
                @endif
                @if($post->texto_detalhe)
                <div class="texto-detalhe">{!! $post->texto_detalhe !!}</div>
                @endif
                <a href="{{ route('blog') }}" class="btn-voltar">« VOLTAR</a>
            </div>

            <div class="imagens-post">
                @foreach($imagens as $imagem)
                <img src="{{ asset('assets/img/blog/posts/imagens/'.$imagem->imagem) }}" alt="" class="img-post">
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection