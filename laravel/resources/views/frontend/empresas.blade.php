@extends('frontend.common.servico_template')

@section('content')

<div class="empresas">
    @foreach($empresas as $empresa)
    
    <section class="empresa center" id="{{$empresa->slug_pt}}">
        <img src="{{ asset('assets/img/empresas/'.$empresa->capa) }}" alt="{{ tobj($empresa, 'titulo') }}" class="img-capa">
        <div class="informacoes">
            <div class="sobre">
                <div class="logo"></div>
                <div class="texto">{!! tobj($empresa, 'texto') !!}</div>
            </div>
            <div class="servicos-contatos">
                <div class="servicos">
                    <h4 class="titulo">{{ t('empresas.servicos') }}</h4>
                    @foreach($servicos->where('empresa_id', $empresa->id) as $servico)

                  

                    <ul style="list-style-image: url({{ asset('assets/img/li.png') }});">
                        <li class="servico">{{ tobj($servico, 'titulo_servico') }}</li>
                    </ul>

                    @endforeach
                </div>
                <div class="contatos">
                    <div class="website">
                        <a href="{{ $empresa->link_website }}" target="_blank" class="link-website" title="{{ t('home.website') }}">
                            {{ t('empresas.website') }} »
                        </a>
                    </div>
                    <div class="contato">
                        <a href="{{ $empresa->link_contato }}" target="_blank" class="link-contato" title="{{ t('home.fale-com') }}">
                            {{ t('empresas.fale-com') }} »
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endforeach
</div>

@endsection