@extends('frontend.common.template')

@section('content')

    <div class="consultor">
        <div class="center">
            <h1>{{ t('nav.area-do-consultor') }}</h1>
            <p>
                {!! t('consultor.texto') !!}
            </p>

            <div class="links">
              
                <a href="https://webmail.edc.eng.br" target="_blank">
                    <img src="{{ asset('assets/img/layout/consultores-webmail.svg') }}" alt="{{ t('consultor.webmail') }}">
                    <h2>{{ t('consultor.webmail') }}</h2>
                    <p>{{ t('consultor.webmail-texto') }}</p>
                </a>

                <a href="https://www.folhasinergyrh.com.br/Login" target="_blank">
                    <img src="{{ asset('assets/img/layout/consultores-holerite.svg') }}" alt="{{ t('consultor.holerite') }}">
                    <h2>{{ t('consultor.holerite') }}</h2>
                    <p>{{ t('consultor.holerite-texto') }}</p>
                </a>

                <a href="https://www.edcacademy.com.br/lms/#/biblioteca" target="_blank">
                    <img src="{{ asset('assets/img/layout/consultores-manuais.svg') }}" alt="{{ t('consultor.manuais') }}">
                    <h2>{{ t('consultor.manuais') }}</h2>
                   {{--  <p>{{ t('consultor.manuais-texto') }}</p> --}}
                   <p>Acesse a biblioteca da EDC Academy e confira os materiais institucionais que irão informar tudo sobre o seu relacionamento com a EDC Group</p>
                </a>

              

            </div>


            <div class='b-box'>
               
                <div>  
                     <a href="https://www.edcacademy.com.br/ " target="_blank"> 
                    
                        <img src="{{ asset('assets/img/layout/marca-edcacademy.svg') }}" alt="">
                        {{-- <h2>{{ t('consultor.manuais') }}</h2> --}}
                    {{--  <p>{{ t('consultor.manuais-texto') }}</p> --}}
                    <p>Acesse regulamente a <b>EDC Academy.</b><br /> Ela foi criada para reunir em um só lugar tudo sobre informações institucionais, treinamentos e aprimoramento pessoal para os funcionários da EDC Group e mais!<br /> Confira agora.</p>
                </a>
                </div>
               
            </div>

            @if($contratacao->texto)
           {{--  <div class="contratacao">{!! $contratacao->texto !!}</div> --}}
            @endif
        </div>
    </div>

@endsection
