@extends('frontend.common.quemSomos_template')

@section('content')

<div class="quem-somos">

    <section class="banner center" style="background-image: url({{ asset('assets/img/quem-somos/'.$quemSomos->banner) }})">
        <div class="textos">
            <h1 class="edc">EDC GROUP</h1>
            <h2 class="titulo-banner">{{ tobj($quemSomos, 'titulo_banner') }}</h2>
            <div class="texto-banner">{!! tobj($quemSomos, 'texto_banner') !!}</div>
        </div>
    </section>

    <section class="empresas center">

        @if(app()->getLocale() === 'en')
         <img src="{{ asset('selo-12anos-eng.svg') }}" class="img-selo"   alt="12 anos">
        @endif

        @if(app()->getLocale() === 'pt')
        <img src="{{ asset('assets/img/layout/selo12anos-pt.svg') }}" class="img-selo" alt="12 anos">
        @endif

        @if(app()->getLocale() === 'es')
        <img src="{{ asset('selo-12anos-esp.svg') }}" class="img-selo"   alt="12 anos">
        @endif


        

        <p class="slogan">{{ tobj($quemSomos, 'slogan') }}</p>
        <div class="links-empresas">
            <a href="" class="link-empresa" target="_blank" title="EDC Serviços">
                <img src="{{ asset('assets/img/layout/marca-edc-servicos.svg') }}" class="img-logo" alt="EDC Serviços">
            </a>
            <a href="" class="link-empresa" target="_blank" title="EDC Engenharia">
                <img src="{{ asset('assets/img/layout/marca-edc-engenharia.svg') }}" class="img-logo" alt="EDC Engenharia">
            </a>
            <a href="" class="link-empresa" target="_blank" title="EDC UNI">
                <img src="{{ asset('assets/img/layout/marca-edc-uni.svg') }}" class="img-logo" alt="EDC UNI">
            </a>
        </div>
    </section>

    <section class="perfil-atuacao center">
        <h2 class="titulo-perfil">{{ t('quem-somos.perfil') }}</h2>
        <div class="atuacoes">
            <div class="item">
                <img src="{{ asset('assets/img/layout/icone-perfil-1.svg') }}" class="img-atuacao">
                {!! tobj($quemSomos, 'item1_atuacao') !!}
            </div>
            <div class="item">
                <img src="{{ asset('assets/img/layout/icone-perfil-2.svg') }}" class="img-atuacao">
                {!! tobj($quemSomos, 'item2_atuacao') !!}
            </div>
            <div class="item">
                <img src="{{ asset('assets/img/layout/icone-perfil-3.svg') }}" class="img-atuacao">
                {!! tobj($quemSomos, 'item3_atuacao') !!}
            </div>
            <div class="item">
                <img src="{{ asset('assets/img/layout/icone-perfil-4.svg') }}" class="img-atuacao">
                {!! tobj($quemSomos, 'item4_atuacao') !!}
            </div>
            <div class="item item5">
                <img src="{{ asset('assets/img/layout/icone-perfil-5.svg') }}" class="img-atuacao">
                {!! tobj($quemSomos, 'item5_atuacao') !!}
            </div>
        </div>
    </section>

    <section class="video">
        <iframe class="video" src="{{ 'https://www.youtube.com/embed/'.$quemSomos->video }}"></iframe>
    </section>

   

    <section class="missao-visao-valores center">
        <div class="missao">
            <h4 class="titulo">{{ t('quem-somos.missao') }}</h4>
            <div class="texto">{!! tobj($quemSomos, 'texto_missao') !!}</div>
        </div>
        <div class="visao">
            <h4 class="titulo">{{ t('quem-somos.visao') }}</h4>
            <div class="texto">{!! tobj($quemSomos, 'texto_visao') !!}</div>
        </div>
        <div class="valores">
            <h4 class="titulo">{{ t('quem-somos.valores') }}</h4>
            <div class="texto">{!! tobj($quemSomos, 'texto_valores') !!}</div>
        </div>
    </section>

    <section class="metodologia center">
        <div class="left">
            <h4 class="titulo-metodologia">{{ t('quem-somos.metodologia') }}</h4>
            <div class="texto-metodologia">{!! tobj($quemSomos, 'texto_metodologia') !!}</div>
        </div>
        <div class="right">
            <img src="{{ asset('assets/img/layout/selo-iso9001.png') }}" alt="" class="img-selo">
            <p class="frase">{{ tobj($quemSomos, 'frase1_certificacao') }}</p>
            <p class="frase">{{ tobj($quemSomos, 'frase2_certificacao') }}</p>
            <p class="frase">{{ tobj($quemSomos, 'frase3_certificacao') }}</p>
        </div>
    </section>

    <section class="politica-qualidade center">
        <h2 class="titulo-politica">{!! tobj($quemSomos, 'titulo_qualidade') !!}</h2>
        <!-- <h2 class="titulo-politica">{{ t('quem-somos.politica') }}</h2> -->
        <div class="texto-politica">
            {!! tobj($quemSomos, 'politica_qualidade') !!}
        </div>
    </section>

    <section class="lideranca">
        <div class="center">
            <h2 class="titulo-lideranca">{{ t('quem-somos.lideranca') }}</h2>
            <div class="lideres">
                @foreach($lideres as $lider)
                <div class="lider">
                    <img src="{{ asset('assets/img/nossa-lideranca/'.$lider->foto) }}" alt="{{ $lider->nome }}" class="img-lider">
                    <p class="nome">{{ $lider->nome }}</p>
                    <p class="cargo">{{ tobj($lider, 'cargo') }}</p>
                    <div class="texto">{!! tobj($lider, 'texto') !!}</div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="clientes center">
        <h2 class="titulo-clientes">{{ t('quem-somos.clientes') }}</h2>
        <div class="grid">
            @foreach($clientes as $c)
            <img src="{{ asset('assets/img/clientes/'.$c->imagem) }}" alt="{{ $c->nome }}">
            @endforeach
        </div>
    </section>

    <section class="estrutura">
        <h2 class="titulo-estrutura">{{ t('quem-somos.estrutura') }}</h2>
        <div class="center">
            <div class="estrutura-texto">

                <img src="{{ asset('assets/img/layout/globo-filiais-edc.png') }}" alt="">

                <div class="textos">
                    <p class="frase-estrutura">{{ tobj($quemSomos, 'frase_estrutura') }}</p>
                    <p>
                        {{ t('estrutura.titulo') }}
                        <span>{{ t('estrutura.sede') }}</span>
                        <span>{{ t('estrutura.filial-br') }}</span>
                        <span>{{ t('estrutura.filial-eua') }}</span>
                        <span>{{ t('estrutura.filial-ar') }}</span>
                    </p>
                </div>
            </div>
            <div class="imagens">
                @foreach($estrutura as $e)
                <a href="{{ asset('assets/img/estrutura/ampliacao/'.$e->imagem) }}" rel="estrutura">
                    <img src="{{ asset('assets/img/estrutura/'.$e->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </section>
</div>

@endsection