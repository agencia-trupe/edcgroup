@extends('frontend.common.contato_template')

@section('content')

<div class="contato">
    <div class="center">
        <div class="informacoes">
            @php $whatsapp = str_replace(" ", "", $contato->whatsapp); @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-whatsapp" target="_blank"><img src="{{ asset('assets/img/layout/icone-whatsapp-contato.svg') }}" alt="" class="img-whatsapp">{{ $contato->whatsapp }}</a>
            <a href="tel:+55{{ $contato->telefone }}" class="link-whatsapp">{{ $contato->telefone }}</a>
            <p class="endereco">{!! tobj($contato, 'endereco') !!}</p>
        </div>

        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}

            <h2>{{ t('contato.fale-conosco') }}</h2>

            @if($errors->any())
            <div class="flash flash-error">{{ t('contato.erro') }}</div>
            @endif
            @if(session('enviado'))
            <div class="flash flash-success">{{ t('contato.enviado') }}</div>
            @endif

            <div class="wrapper">
                <div>
                    <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}" value="{{ old('telefone') }}">

                     <input type="text" name="empresa" placeholder="{{ t('contato.empresa') }}" value="" required>
                    <input type="text" name="cargo" placeholder="{{ t('contato.cargo') }}" value="" required>
                    
                    <div style='background:#fff;color:#000; margin:5px 0px 0px 0px;'>
                    
                    <select name="interesse" id="" class='contato-interesse'>
                        <option value="#">{{ t('contato.interesse') }} (Selecione)</option>
                        <option value="Terceiros e Temporários">{{t('contato.terceiros_temporarios')}}</option>
                        <option value="Outsourcing Especializado">{{t('contato.outsourcing_especializado')}}</option>
                        <option value="Pessoas & Estratégia">{{t('contato.pessoas_estrategias')}}</option>
                        <option value="Outros">{{t('contato.outros')}}</option>
                    </select>
                    </div> 

                </div>
                <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="{{ t('contato.enviar') }}">
            </div>

            <a href="https://www.edcgroup.com.br/candidato" target="_blank">
                <div class="work_for_us">
                    <h1>TRABALHE CONOSCO</h1>
                    <P>Para consultar vagas e enviar seu curriculo acesse a <strong>ÁREA DO CANDIDATO EDC</strong></P>
                </div>
            </a>
            
        </form>
    </div>

    <div class="mapa">{!! $contato->google_maps !!}</div>
</div>

@endsection