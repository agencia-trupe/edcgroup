const config = {
    padrao: {
        toolbar: [['Bold', 'Italic']],
    },

    padraoTitle: {
        format_tags: 'h2;p',
        toolbar: [['Format'], ['Bold', 'Italic']],
    },

    padraoList: {
        toolbar: [['Bold', 'Italic'], ['BulletedList']],
    },

    boldBr: {
        toolbar: [['Bold']],
        enterMode: CKEDITOR.ENTER_BR,
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
    },

    textoHome: {
        format_tags: 'h2;h3;p',
        toolbar: [['Format'], ['Bold', 'Italic']],
    },

    textoServicos: {
        format_tags: 'h2;h3;p',
        toolbar: [['Format'], ['Bold', 'Italic'], ['BulletedList']],
    },

    contratacao: {
        format_tags: 'h2;p',
        toolbar: [['Format'], ['Bold', 'Italic'], ['InjectImage']],
        height: 440,
    },

    novidades: {
        toolbar: [
            ['Bold', 'Italic'],
            ['BulletedList'],
            ['Link', 'Unlink'],
            ['InjectImage'],
        ],
        height: 440,
    },

    politica: {
        toolbar: [['Bold', 'Italic'], ['BulletedList'], ['Link', 'Unlink']],
    },
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        `${$('base').attr('href')}/assets/ckeditor.css`,
        CKEDITOR.config.contentsCss,
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal(
        'injectimage',
        `${$('base').attr('href')}/assets/injectimage/plugin.js`
    );
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage';

    $('.ckeditor').each(function(i, obj) {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
}
