import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import BannerHome from './BannerHome';

AjaxSetup();
MobileToggle();
BannerHome();

$(document).ready(function() {
    $('select[name=lang]').change(function selectRoute() {
        window.location = $(this)
            .find(`option[value=${this.value}]`)
            .data('route');
    });

    $('.estrutura .imagens a').fancybox({
        padding: 0,
    });

    $('.depoimentos-wrapper').cycle({
        slides: '> div',
        autoHeight: 'container',
    });

    $('.novidades-ver-mais').click(function onClick(event) {
        event.preventDefault();

        const $btn = $(this);
        const $container = $('.novidades-index');

        if ($btn.hasClass('loading')) return false;

        $btn.addClass('loading');

        return $.get(
            $btn.data('next'),
            ({ novidades: novidadesHtml, nextPage }) => {
                const novidades = $(novidadesHtml).hide();

                $container.append(novidades);
                novidades.fadeIn();

                $btn.removeClass('loading');

                if (nextPage) {
                    $btn.data('next', nextPage);
                } else {
                    $btn.fadeOut(function fadeOut() {
                        $(this).remove();
                    });
                }
            }
        );
    });

    // home
    $('.home .empresas-servicos .linha-divisao:nth-child(3n)').css(
        'display',
        'none'
    );
    $('.home .novidades .link-novidade:nth-child(3n)').css(
        'border-right',
        'none'
    );

    // AVISO DE COOKIES
    $('.aviso-cookies').hide();

    if (window.location.href == routeHome) {
        $('.aviso-cookies').show();

        $('.aceitar-cookies').click(function() {
            var url = window.location.origin + '/aceite-de-cookies';

            $.ajax({
                type: 'POST',
                url: url,
                success: function(data, textStatus, jqXHR) {
                    $('.aviso-cookies').hide();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                },
            });
        });
    }

    // BLOG
    $('.blog .posts .post:eq(0)').css({
        'padding-top': '0',
        'border-top': 'none',
    });
    $('.blog .posts .post:eq(1)').css({
        'padding-top': '0',
        'border-top': 'none',
    });
    $('.blog .posts .post:eq(2)').css({
        'padding-top': '0',
        'border-top': 'none',
    });
    $('.blog .posts .post:nth-child(3n)').css('margin-right', '0');

    $('.blog-post .imagens-post')
        .masonry({
            itemSelector: '.img-post',
            columnWidth: '.img-post',
            percentPosition: true,
        })
        .on('layoutComplete', function(event, laidOutItems) {
            $('.blog-post .imagens-post').masonry('layout');
        });

    $('.blog-post .imagens-post .img-post:nth-child(2n)').css(
        'margin-right',
        '0'
    );

    // depoimentos candidatos
    $('.area-do-candidato .right').cycle({
        slides: '.depoimento',
        fx: 'fade',
        speed: 800,
        timeout: 10000,
        next: '#next',
    });

    $("#chatWhatsapp").floatingWhatsApp({
        phone: "+55" + whatsappNumero,
        popupMessage:
            "Seja muito bem-vindo(a) à EDC Group. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-edc-uni"><img src="' +
            imgMarcaWhatsapp +
            '" alt=""></div>' +
            '<div class="textos"><p class="titulo">EDC Group</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });
});
