export default function BannerHome() {
    const TIMEOUT = 4000;
    const $navLinks = $('.banner nav a');
    const $images = $('.banner .imagens img');

    let activeIndex = 0;
    let isPaused = false;

    function next(index = null) {
        if (isPaused && index === null) return;

        if (index !== null) {
            activeIndex = index;
        } else {
            activeIndex =
                activeIndex === $navLinks.length - 1 ? 0 : activeIndex + 1;
        }

        [$navLinks, $images].map(el =>
            el
                .removeClass('active')
                .eq(activeIndex)
                .addClass('active')
        );
    }

    $navLinks.hover(
        function mouseEnter() {
            isPaused = true;
            next($navLinks.index($(this)));
        },
        function mouseLeave() {
            isPaused = false;
        }
    );

    setInterval(next, TIMEOUT);
}
