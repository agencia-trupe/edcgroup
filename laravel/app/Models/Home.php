<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_banner()
    {
        return CropImage::make('banner', [
            'width'  => 1420,
            'height' => 500,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_img_vagas()
    {
        return CropImage::make('img_vagas', [
            'width'  => 600,
            'height' => 280,
            'path'   => 'assets/img/home/'
        ]);
    }
}
