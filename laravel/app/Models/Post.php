<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($data)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\PostImagem', 'post_id')->ordenados();
    }

    public function hashtags()
    {
        return $this->hasMany('App\Models\PostHashtag', 'post_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/blog/posts/'
        ]);
    }
}
