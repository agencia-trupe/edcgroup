<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class PostImagem extends Model
{
    protected $table = 'posts_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopePost($query, $id)
    {
        return $query->where('post_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => null,
                'path'    => 'assets/img/blog/posts/imagens/thumbs/'
            ],
            [
                'width'  => 400,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/blog/posts/imagens/'
            ]
        ]);
    }
}
