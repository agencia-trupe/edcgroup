<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Contratacao extends Model
{
    protected $table = 'contratacao';

    protected $guarded = ['id'];

}
