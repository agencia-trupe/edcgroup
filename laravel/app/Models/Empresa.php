<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';

    protected $guarded = ['id'];


    public function servicos()
    {
        return $this->hasMany('App\Models\EmpresaServicos', 'empresa_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1200,
            'height' => null,
            'path'   => 'assets/img/empresas/'
        ]);
    }
}
