<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Estrutura extends Model
{
    protected $table = 'estrutura';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 200,
                'height' => 125,
                'path'   => 'assets/img/estrutura/'
            ],
            [
                'width'  => 960,
                'height' => null,
                'path'   => 'assets/img/estrutura/ampliacao/'
            ],
        ]);
    }
}
