<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaServicos extends Model
{
    protected $table = 'empresas_servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeEmpresa($query, $id)
    {
        return $query->where('empresa_id', $id);
    }

    public function scopeHome($query)
    {
        return $query->where('home', '=', 1);
    }
}
