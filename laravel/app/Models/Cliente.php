<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'       => 240,
            'height'      => 90,
            'transparent' => true,
            'upsize'      => true,
            'path'        => 'assets/img/clientes/'
        ]);
    }
}
