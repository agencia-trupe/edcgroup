<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepoimentoCandidato extends Model
{
    protected $table = 'depoimentos_candidatos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
