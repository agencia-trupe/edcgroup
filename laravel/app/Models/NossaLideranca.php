<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class NossaLideranca extends Model
{
    protected $table = 'nossa_lideranca';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 400,
            'height' => 400,
            'path'   => 'assets/img/nossa-lideranca/'
        ]);
    }
}
