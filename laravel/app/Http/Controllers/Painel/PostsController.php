<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRequest;
use App\Models\Post;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('data', 'DESC')->get();

        return view('painel.blog.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('painel.blog.posts.create');
    }

    public function store(PostsRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            Post::create($input);

            return redirect()->route('painel.blog.posts.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Post $post)
    {
        return view('painel.blog.posts.edit', compact('post'));
    }

    public function update(PostsRequest $request, Post $post)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            $post->update($input);

            return redirect()->route('painel.blog.posts.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Post $post)
    {
        try {
            $post->delete();

            return redirect()->route('painel.blog.posts.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
