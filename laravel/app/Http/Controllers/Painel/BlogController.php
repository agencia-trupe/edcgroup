<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $blog = Blog::first();

        return view('painel.blog.pagina.edit', compact('blog'));
    }

    public function update(BlogRequest $request, $blog)
    {
        try {
            $input = $request->all();
            $blog = Blog::where('id', $blog)->first();

            $blog->update($input);

            return redirect()->route('painel.blog.pagina.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
