<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresasRequest;
use App\Models\Empresa;

class EmpresasController extends Controller
{
    public function index()
    {
        $empresas = Empresa::orderBy('id', 'ASC')->get();

        return view('painel.empresas.index', compact('empresas'));
    }

    public function edit(Empresa $empresa)
    {
        return view('painel.empresas.edit', compact('empresa'));
    }

    public function update(EmpresasRequest $request, Empresa $empresa)
    {
        try {
            $input = $request->all();
            $input['slug_pt'] = str_slug($request->titulo_pt, "-");

            if (isset($input['titulo_en'])) $input['slug_en'] = str_slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = str_slug($request->titulo_es, "-");
            if (isset($input['capa'])) $input['capa'] = Empresa::upload_capa();

            $empresa->update($input);

            return redirect()->route('painel.empresas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
