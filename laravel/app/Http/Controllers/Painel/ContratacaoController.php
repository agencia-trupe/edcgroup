<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContratacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Contratacao;

class ContratacaoController extends Controller
{
    public function index()
    {
        $registro = Contratacao::first();

        return view('painel.contratacao.edit', compact('registro'));
    }

    public function update(ContratacaoRequest $request, Contratacao $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.contratacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
