<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresasServicosRequest;
use App\Models\Empresa;
use App\Models\EmpresaServicos;

class EmpresasServicosController extends Controller
{
    public function index(Empresa $empresa)
    {
        $servicos = EmpresaServicos::empresa($empresa->id)->ordenados()->get();

        return view('painel.empresas.servicos.index', compact('servicos', 'empresa'));
    }

    public function create(Empresa $empresa)
    {
        return view('painel.empresas.servicos.create', compact('empresa'));
    }

    public function store(EmpresasServicosRequest $request, Empresa $empresa)
    {
        try {
            $input = $request->all();
            $input['empresa_id'] = $empresa->id;

            EmpresaServicos::create($input);

            return redirect()->route('painel.empresas.servicos.index', $empresa->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Empresa $empresa, EmpresaServicos $servico)
    {
        return view('painel.empresas.servicos.edit', compact('empresa', 'servico'));
    }

    public function update(EmpresasServicosRequest $request, Empresa $empresa, EmpresaServicos $servico)
    {
        try {
            $input = $request->all();
            $input['empresa_id'] = $empresa->id;

            $servico->update($input);

            return redirect()->route('painel.empresas.servicos.index', $empresa->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Empresa $empresa, EmpresaServicos $servico)
    {
        try {
            $servico->delete();

            return redirect()->route('painel.empresas.servicos.index', $empresa->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
