<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NossaLiderancaRequest;
use App\Models\NossaLideranca;

class NossaLiderancaController extends Controller
{
    public function index()
    {
        $registros = NossaLideranca::ordenados()->get();

        return view('painel.nossa-lideranca.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.nossa-lideranca.create');
    }

    public function store(NossaLiderancaRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = NossaLideranca::upload_foto();

            NossaLideranca::create($input);

            return redirect()->route('painel.nossa-lideranca.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(NossaLideranca $registro)
    {
        return view('painel.nossa-lideranca.edit', compact('registro'));
    }

    public function update(NossaLiderancaRequest $request, NossaLideranca $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = NossaLideranca::upload_foto();

            $registro->update($input);

            return redirect()->route('painel.nossa-lideranca.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(NossaLideranca $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.nossa-lideranca.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
