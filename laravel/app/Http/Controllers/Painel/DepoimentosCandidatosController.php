<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepoimentosCandidatosRequest;
use App\Models\DepoimentoCandidato;

class DepoimentosCandidatosController extends Controller
{
    public function index()
    {
        $depoimentos = DepoimentoCandidato::ordenados()->get();

        return view('painel.depoimentos-candidatos.index', compact('depoimentos'));
    }

    public function create()
    {
        return view('painel.depoimentos-candidatos.create');
    }

    public function store(DepoimentosCandidatosRequest $request)
    {
        try {
            $input = $request->all();

            DepoimentoCandidato::create($input);

            return redirect()->route('painel.depoimentos-candidatos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(DepoimentoCandidato $depoimento)
    {
        return view('painel.depoimentos-candidatos.edit', compact('depoimento'));
    }

    public function update(DepoimentosCandidatosRequest $request, DepoimentoCandidato $depoimento)
    {
        try {
            $input = $request->all();

            $depoimento->update($input);

            return redirect()->route('painel.depoimentos-candidatos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(DepoimentoCandidato $depoimento)
    {
        try {
            $depoimento->delete();

            return redirect()->route('painel.depoimentos-candidatos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
