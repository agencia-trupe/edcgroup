<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostsImagensRequest;
use App\Models\Post;
use App\Models\PostImagem;

class PostsImagensController extends Controller
{
    public function index(Post $post)
    {
        $imagens = PostImagem::post($post->id)->ordenados()->get();

        return view('painel.blog.posts.imagens.index', compact('imagens', 'post'));
    }

    public function show(Post $post, PostImagem $imagem)
    {
        return $imagem;
    }

    public function store(Post $post, PostsImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = PostImagem::uploadImagem();
            $input['post_id'] = $post->id;

            $count = count(PostImagem::where('post_id', $post->id)->get());

            if ($count < 10) {
                $imagem = PostImagem::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique a quantidade de imagens adicionadas (limite: 10 imagens)']);
            }

            $view = view('painel.blog.posts.imagens.imagem', compact('post', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Post $post, PostImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.blog.posts.imagens.index', $post)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Post $post)
    {
        try {
            $post->imagens()->delete();

            return redirect()->route('painel.blog.posts.imagens.index', $post)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }
}
