<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\AceiteDeCookies;
use App\Models\Home;
use App\Models\Servico;
use App\Models\Cliente;
use App\Models\Depoimento;
use App\Models\Empresa;
use App\Models\EmpresaServicos;
use App\Models\Novidade;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $novidades = Novidade::orderBy('data', 'DESC')->whereIdioma(app()->getLocale())->publicados()
            ->join('novidades_categorias', 'novidades_categorias.id', '=', 'novidades.novidades_categoria_id')
            ->select('novidades_categorias.id as novidades_categoria_id', 'novidades_categorias.titulo_pt as titulo_pt', 'novidades_categorias.titulo_en as titulo_en', 'novidades_categorias.titulo_es as titulo_es', 'novidades_categorias.slug as categoria_slug', 'novidades.id as id', 'novidades.data as data', 'novidades.titulo as titulo', 'novidades.capa as capa', 'novidades.slug as slug')
            ->take(3)->get();
        
        $empresas = Empresa::orderBy('id', 'ASC')->get();
        $servicos = EmpresaServicos::home()->ordenados()->get();
        $home = Home::first();
        $depoimentos = Depoimento::ordenados()->get();

        // dd($novidades);

        return view('frontend.home', [
            'home'        => $home,
            'empresas'    => $empresas,
            'servicos'    => $servicos,
            'depoimentos' => $depoimentos,
            'verificacao' => AceiteDeCookies::where('ip', $request->ip())->first(),
            'novidades'   => $novidades,
        ]);
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }

    public function openPdf()
    {
        $arquivo = "apresentacao-programa-trainees-EDC.pdf";
        
        $arquivoPath = public_path() . "/assets/pdf-trainees/" . $arquivo;

        return response()->file($arquivoPath);
    }

}
