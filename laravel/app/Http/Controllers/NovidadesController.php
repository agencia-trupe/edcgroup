<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Novidade;
use App\Models\NovidadeCategoria;

class NovidadesController extends Controller
{
    public function __construct()
    {
        view()->share(
            'categorias',
            NovidadeCategoria::whereHas('novidades', function($query) {
                $query
                    ->where('publicado', 1)
                    ->where('idioma', app()->getLocale());
            })->ordenados()->get()
        );
    }

    public function index()
    {
        if (request()->get('page') && !request()->ajax()) abort('404');

        $novidades = Novidade::ordenados()
            ->whereIdioma(app()->getLocale())
            ->publicados()
            ->paginate(14);

        if (request()->ajax()) {
            return [
                'novidades' => view()->make('frontend.novidades._ajax', compact('novidades'))->render(),
                'nextPage'  => $novidades->hasMorePages()
                    ? route('novidades', ['page' => $novidades->currentPage() + 1])
                    : null
            ];
        }

        return view('frontend.novidades.index', compact('novidades'));
    }

    public function busca()
    {
        if (!$termo = request()->get('q')) abort('404');

        $novidades = Novidade::ordenados()
            ->busca($termo)
            ->whereIdioma(app()->getLocale())
            ->publicados()
            ->paginate(10);

        return view('frontend.novidades.lista', compact('novidades'));
    }

    public function categoria(NovidadeCategoria $categoria)
    {
        $novidades = $categoria->novidades()
            ->ordenados()
            ->whereIdioma(app()->getLocale())
            ->publicados()
            ->paginate(10);

        return view('frontend.novidades.lista', compact('novidades', 'categoria'));
    }

    public function show(NovidadeCategoria $categoria, Novidade $novidade)
    {
        if (!$novidade->publicado || $novidade->categoria->id !== $categoria->id)
            abort('404');

        $relacionados = $novidade->categoria->novidades()
            ->ordenados()
            ->where('id', '!=', $novidade->id)
            ->whereIdioma(app()->getLocale())
            ->publicados()
            ->limit(3)
            ->get();

        return view('frontend.novidades.show', compact('novidade', 'relacionados'));
    }
}
