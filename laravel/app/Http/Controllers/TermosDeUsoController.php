<?php

namespace App\Http\Controllers;

use App\Models\TermosDeUso;

class TermosDeUsoController extends Controller
{
    public function index()
    {
        return view('frontend.termos', [
            'termos' => TermosDeUso::first()
        ]);
    }
}
