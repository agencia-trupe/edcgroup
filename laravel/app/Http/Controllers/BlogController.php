<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Blog;
use App\Models\Post;
use App\Models\PostHashtag;
use App\Models\PostImagem;

class BlogController extends Controller
{
    public function index()
    {
        $dataHoje = date("Y-m-d");
        $blog = Blog::first();
        $posts = Post::where('data', '<=', $dataHoje)->orderBy('data', 'desc')->get();
        $imagens = PostImagem::ordenados()->get();

        return view('frontend.blog', compact('blog', 'posts', 'imagens'));
    }

    public function show(Post $post)
    {
        $blog = Blog::first();
        $imagens = PostImagem::post($post->id)->ordenados()->get();

        return view('frontend.blog-post', compact('post', 'blog', 'imagens'));
    }
}
