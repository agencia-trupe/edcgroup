<?php

namespace App\Http\Controllers;

use App\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        return view('frontend.politica', [
            'politica' => PoliticaDePrivacidade::first()
        ]);
    }
}
