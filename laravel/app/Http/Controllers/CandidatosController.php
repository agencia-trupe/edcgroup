<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\DepoimentoCandidato;

class CandidatosController extends Controller
{
    public function index()
    {
        $depoimentos = DepoimentoCandidato::ordenados()->get();

        return view('frontend.candidato', compact('depoimentos'));
    }
}
