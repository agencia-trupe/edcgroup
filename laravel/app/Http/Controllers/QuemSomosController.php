<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Estrutura;
use App\Models\NossaLideranca;
use App\Models\QuemSomos;

class QuemSomosController extends Controller
{
    public function index()
    {
        return view('frontend.quem-somos', [
            'quemSomos' => QuemSomos::first(),
            'clientes'  => Cliente::ordenados()->get(),
            'estrutura' => Estrutura::ordenados()->get(),
            'lideres'   => NossaLideranca::ordenados()->get(),
        ]);
    }
}
