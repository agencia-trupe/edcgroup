<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Contato;
use App\Models\Newsletter;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function post(Request $request, Newsletter $newsletter)
    {
        $data = $request->all();

        $newsletter->create($data);

        return redirect('/')->with('enviado', true);
    }
}
