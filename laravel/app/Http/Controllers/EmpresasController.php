<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Empresa;
use App\Models\EmpresaServicos;

class EmpresasController extends Controller
{
    public function index()
    {
        return view('frontend.empresas', [
            'empresas' => Empresa::orderBy('id', 'asc')->get(),
            'servicos' => EmpresaServicos::ordenados()->get(),
        ]);
    }
}
