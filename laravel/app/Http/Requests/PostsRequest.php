<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data'          => 'required',
            'texto_post'    => 'required',
            'texto_detalhe' => '',
            'capa'          => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
