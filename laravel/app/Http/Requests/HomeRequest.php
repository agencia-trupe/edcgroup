<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'banner'          => 'image',
            'frase_banner_pt' => 'required',
            'img_vagas'       => 'image',
            'frase_vagas_pt'  => 'required',
            'video'           => 'required',
        ];
    }
}
