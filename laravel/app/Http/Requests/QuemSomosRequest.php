<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuemSomosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'banner'                 => 'image',
            'titulo_banner_pt'       => 'required',
            'texto_banner_pt'        => 'required',
            'slogan_pt'              => 'required',
            'item1_atuacao_pt'       => 'required',
            'item2_atuacao_pt'       => 'required',
            'item3_atuacao_pt'       => 'required',
            'item4_atuacao_pt'       => 'required',
            'item5_atuacao_pt'       => 'required',
            'video'                  => '',
            'texto_missao_pt'        => 'required',
            'texto_visao_pt'         => 'required',
            'texto_valores_pt'       => 'required',
            'texto_metodologia_pt'   => 'required',
            'frase1_certificacao_pt' => 'required',
            'frase2_certificacao_pt' => 'required',
            'frase3_certificacao_pt' => 'required',
            'politica_qualidade_pt'  => 'required',
            'frase_estrutura_pt'     => 'required',
        ];
    }
}
