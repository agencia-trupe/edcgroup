<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosCandidatosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'          => 'required',
            'depoimento_pt' => 'required',
            'depoimento_en' => '',
            'depoimento_es' => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
