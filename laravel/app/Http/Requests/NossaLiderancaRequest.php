<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NossaLiderancaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'     => 'required',
            'cargo_pt' => 'required',
            'cargo_en' => '',
            'cargo_es' => '',
            'foto'     => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => '',
            'texto_es' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
