<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'depoimento_pt' => 'required',
            'depoimento_en' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
