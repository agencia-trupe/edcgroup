<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NovidadesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'novidades_categoria_id' => 'required',
            'data' => 'required',
            'idioma' => 'required|in:pt,en,es',
            'titulo' => 'required',
            'capa' => 'required|image',
            'texto' => 'required',
            'publicado' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
