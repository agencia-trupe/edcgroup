<?php

use Illuminate\Support\Facades\Lang;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresas', 'EmpresasController@index')->name('empresas');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('novidades', 'NovidadesController@index')->name('novidades');
    Route::get('novidades/busca', 'NovidadesController@busca')->name('novidades.busca');
    Route::get('novidades/{novidade_categoria_slug}', 'NovidadesController@categoria')->name('novidades.categoria');
    Route::get('novidades/{novidade_categoria_slug}/{novidade_slug}', 'NovidadesController@show')->name('novidades.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    // Route::get('contatos', 'ContatoController@index2')->name('contatos');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('termos-de-uso', 'TermosDeUsoController@index')->name('termos-de-uso');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::get('candidato', 'CandidatosController@index')->name('candidato');
    Route::get('trainees/pdf', 'HomeController@openPdf')->name('trainees.pdf');
    Route::get('trainees', function() {
        return view('frontend.trainees');
    })->name('trainees');
    Route::get('consultor', function() {
        return view('frontend.consultor', ['contratacao' => \App\Models\Contratacao::first()]);
    })->name('consultor');

    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');
    Route::post('newsletter', 'NewsletterController@post')->name('newsletter.post');

    Route::get('blog', 'BlogController@index')->name('blog');
    Route::get('blog/{post}', 'BlogController@show')->name('post.detalhes');

    // Internacionalização
    Route::get('lang/{lang}', function($lang) {
        if (in_array($lang, ['pt', 'en', 'es'])) {
            Session::put('locale', $lang);
        }
        return back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('empresas', 'EmpresasController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('empresas.servicos', 'EmpresasServicosController');
		Route::resource('novidades/categorias', 'NovidadesCategoriasController', ['parameters' => ['categorias' => 'categorias_novidades']]);
		Route::resource('novidades', 'NovidadesController');
		Route::resource('contratacao', 'ContratacaoController', ['only' => ['index', 'update']]);
		Route::resource('depoimentos-candidatos', 'DepoimentosCandidatosController');
		Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('nossa-lideranca', 'NossaLiderancaController');
		Route::resource('estrutura', 'EstruturaController');
		Route::resource('clientes', 'ClientesController');
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
		Route::resource('termos-de-uso', 'TermosDeUsoController', ['only' => ['index', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::get('newsletter', 'NewsletterController@index')->name('painel.newsletter.index');
        Route::get('newsletter/export', 'NewsletterController@export')->name('painel.newsletter.export');

		Route::resource('blog/pagina', 'BlogController', ['only' => ['index', 'update']]);
		Route::resource('blog/posts', 'PostsController');
        Route::get('blog/posts/{post}/imagens/clear', [
            'as'   => 'painel.blog.posts.imagens.clear',
            'uses' => 'PostsImagensController@clear'
        ]);
		Route::resource('blog/posts.imagens', 'PostsImagensController', ['parameters' => ['imagens' => 'posts_imagens']]);

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
