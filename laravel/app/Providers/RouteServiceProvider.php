<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
		$router->model('servicos', 'App\Models\EmpresaServicos');
		$router->model('empresas', 'App\Models\Empresa');
		$router->model('depoimentos_candidatos', 'App\Models\DepoimentoCandidato');
		$router->model('posts_imagens', 'App\Models\PostImagem');
		$router->model('posts', 'App\Models\Post');
		$router->model('blog', 'App\Models\Blog');
		$router->model('novidades', 'App\Models\Novidade');
		$router->model('categorias_novidades', 'App\Models\NovidadeCategoria');
		$router->model('contratacao', 'App\Models\Contratacao');
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('home', 'App\Models\Home');
		$router->model('estrutura', 'App\Models\Estrutura');
		$router->model('clientes', 'App\Models\Cliente');
		$router->model('nossa_lideranca', 'App\Models\NossaLideranca');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
		$router->model('termos-de-uso', 'App\Models\TermosDeUso');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        	$router->model('recebidos', 'App\Models\ContatoRecebido');
        	$router->model('contato', 'App\Models\Contato');
        	$router->model('usuarios', 'App\Models\User');
        	$router->model('aceite-de-cookies', 'App\Models\AceiteDeCookies');
        	$router->model('newsletter', 'App\Models\Newsletter');

        $router->bind('novidade_categoria_slug', function($value) {
            return \App\Models\NovidadeCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('novidade_slug', function($value) {
            return \App\Models\Novidade::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
