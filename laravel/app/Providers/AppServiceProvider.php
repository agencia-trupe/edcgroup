<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Empresa;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        // view()->composer('frontend.common.template', function ($view) {
        //     $view->with('contato', \App\Models\Contato::first());
        //     $view->with('blog', \App\Models\Blog::first());
        //     $view->with('edcServicos', \App\Models\Empresa::where('id', 1)->first());
        //     $view->with('edcEngenharia', \App\Models\Empresa::where('id', 2)->first());
        //     $view->with('edcUni', \App\Models\Empresa::where('id', 3)->first());
        // });

        view()->composer('frontend.common.template', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('blog', \App\Models\Blog::first());
            $view->with('edcServicos', Empresa::where('id', 1)->first());
            $view->with('edcEngenharia', Empresa::where('id', 2)->first());
            $view->with('edcUni', Empresa::where('id', 3)->first());
        });

        view()->composer('painel.common.nav', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('frontend.blog*', function () {
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
