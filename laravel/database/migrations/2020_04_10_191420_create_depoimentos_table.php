<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosTable extends Migration
{
    public function up()
    {
        Schema::create('depoimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->text('depoimento_pt');
            $table->text('depoimento_en');
            $table->text('depoimento_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('depoimentos');
    }
}
