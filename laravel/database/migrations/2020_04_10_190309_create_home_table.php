<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner');
            $table->string('frase_banner_pt');
            $table->string('frase_banner_en');
            $table->string('frase_banner_es');
            $table->string('img_vagas');
            $table->string('frase_vagas_pt');
            $table->string('frase_vagas_en');
            $table->string('frase_vagas_es');
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
