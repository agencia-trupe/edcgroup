<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data');
            $table->string('capa');
            $table->text('texto_post');
            $table->text('texto_detalhe')->nullable();
            $table->text('hashtags')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('posts');
    }
}
