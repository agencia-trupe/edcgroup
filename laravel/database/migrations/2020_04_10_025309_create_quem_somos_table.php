<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('banner');
            $table->string('titulo_banner_pt');
            $table->string('titulo_banner_en');
            $table->string('titulo_banner_es');
            $table->text('texto_banner_pt');
            $table->text('texto_banner_en');
            $table->text('texto_banner_es');
            $table->string('slogan_pt');
            $table->string('slogan_en');
            $table->string('slogan_es');
            $table->text('item1_atuacao_pt');
            $table->text('item1_atuacao_en');
            $table->text('item1_atuacao_es');
            $table->text('item2_atuacao_pt');
            $table->text('item2_atuacao_en');
            $table->text('item2_atuacao_es');
            $table->text('item3_atuacao_pt');
            $table->text('item3_atuacao_en');
            $table->text('item3_atuacao_es');
            $table->text('item4_atuacao_pt');
            $table->text('item4_atuacao_en');
            $table->text('item4_atuacao_es');
            $table->text('item5_atuacao_pt');
            $table->text('item5_atuacao_en');
            $table->text('item5_atuacao_es');
            $table->string('video')->nullable();
            $table->text('texto_missao_pt');
            $table->text('texto_missao_en');
            $table->text('texto_missao_es');
            $table->text('texto_visao_pt');
            $table->text('texto_visao_en');
            $table->text('texto_visao_es');
            $table->text('texto_valores_pt');
            $table->text('texto_valores_en');
            $table->text('texto_valores_es');
            $table->text('texto_metodologia_pt');
            $table->text('texto_metodologia_en');
            $table->text('texto_metodologia_es');
            $table->string('frase1_certificacao_pt');
            $table->string('frase1_certificacao_en');
            $table->string('frase1_certificacao_es');
            $table->string('frase2_certificacao_pt');
            $table->string('frase2_certificacao_en');
            $table->string('frase2_certificacao_es');
            $table->string('frase3_certificacao_pt');
            $table->string('frase3_certificacao_en');
            $table->string('frase3_certificacao_es');
            $table->text('politica_qualidade_pt');
            $table->text('politica_qualidade_en');
            $table->text('politica_qualidade_es');
            $table->string('frase_estrutura_pt');
            $table->string('frase_estrutura_en');
            $table->string('frase_estrutura_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
