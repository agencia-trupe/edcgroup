<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasServicosTable extends Migration
{
    public function up()
    {
        Schema::create('empresas_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo_servico_pt');
            $table->string('titulo_servico_en');
            $table->string('titulo_servico_es');
            $table->boolean('home')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresas_servicos');
    }
}
