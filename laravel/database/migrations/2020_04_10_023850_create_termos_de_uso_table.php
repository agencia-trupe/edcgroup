<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermosDeUsoTable extends Migration
{
    public function up()
    {
        Schema::create('termos_de_uso', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('termos_de_uso');
    }
}
