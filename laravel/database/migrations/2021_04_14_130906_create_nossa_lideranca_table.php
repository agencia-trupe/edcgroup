<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNossaLiderancaTable extends Migration
{
    public function up()
    {
        Schema::create('nossa_lideranca', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('cargo_pt');
            $table->string('cargo_en');
            $table->string('cargo_es');
            $table->string('foto');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('nossa_lideranca');
    }
}
