<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(ContratacaoSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(QuemSomosSeeder::class);
		$this->call(PoliticaDePrivacidadeSeeder::class);
		$this->call(TermosDeUsoSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(BlogTableSeeder::class);
        $this->call(EmpresasSeeder::class);

        Model::reguard();
    }
}
