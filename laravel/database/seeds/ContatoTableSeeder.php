<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@edcgroup.com.br',
            'telefone' => '+55 11 3292 6444',
            'whatsapp' => '+55 19 99439 4953',
            'endereco_pt' => 'SEDE · Rua José Bonifácio, 24 - 1º andar<br />Sé - São Paulo, SP · Brasil · 01003-900',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.5446474516048!2d-46.6367906850224!3d-23.548874684689107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59ab2647593f%3A0x207dd8a82eeee391!2zUi4gSm9zw6kgQm9uaWbDoWNpbywgMjQgLSBTw6ksIFPDo28gUGF1bG8gLSBTUCwgMDEwMDMtMDAw!5e0!3m2!1spt-BR!2sbr!4v1586624699297!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
            'instagram' => 'https://www.instagram.com/edcgrouprh/',
            'facebook'  => 'https://www.facebook.com/edcgrupo',
            'linkedin'  => 'https://www.linkedin.com/company/edcgroup-br/',
            'link_vagas'  => 'https://www.portalsinergyrh.com.br/Portal/MeuPortal/MeuPortal?empresa=1515&master=0',
        ]);
    }
}
