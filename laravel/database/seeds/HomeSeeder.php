<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'banner' => '',
            'frase_banner_pt' => 'EDC GROUP APRESENTA AS EMPRESAS DO GRUPO:',
            'frase_banner_en' => '',
            'frase_banner_es' => '',
            'img_vagas'       => '',
            'frase_vagas_pt' => 'Encontre uma OPORTUNIDADE PROFISSIONAL. Cadastre-se em nosso Banco de Talentos.',
            'frase_vagas_en' => '',
            'frase_vagas_es' => '',
            'video' => 'oW1GnuIoo7A',
        ]);
    }
}
