<?php

use Illuminate\Database\Seeder;

class EmpresasSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresas')->insert([
            'id' => 1,
            'capa' => '',
            'slug_pt' => 'edc-servicos',
            'slug_en' => 'edc-services',
            'slug_es' => 'servicios-edc',
            'titulo_pt' => 'EDC Serviços',
            'titulo_en' => 'EDC Services',
            'titulo_es' => 'Servicios EDC',
            'link_website' => '',
            'link_contato' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);

        DB::table('empresas')->insert([
            'id' => 2,
            'capa' => '',
            'slug_pt' => 'edc-engenharia',
            'slug_en' => 'edc-engineering',
            'slug_es' => 'ingenieria-edc',
            'titulo_pt' => 'EDC Engenharia',
            'titulo_en' => 'EDC Engineering',
            'titulo_es' => 'Ingeniería EDC',
            'link_website' => '',
            'link_contato' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);

        DB::table('empresas')->insert([
            'id' => 3,
            'capa' => '',
            'slug_pt' => 'edc-uni',
            'slug_en' => 'edc-uni',
            'slug_es' => 'edc-uni',
            'titulo_pt' => 'EDC UNI',
            'titulo_en' => 'EDC UNI',
            'titulo_es' => 'EDC UNI',
            'link_website' => '',
            'link_contato' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);
    }
}
