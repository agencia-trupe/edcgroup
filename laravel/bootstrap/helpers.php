<?php

    function t($termo, $replacements = []) {
        return trans('frontend.'.$termo, $replacements);
    }

    function tobj($obj, $termo)
    {
        return
            $obj->{$termo.'_'.app()->getLocale()} ?:
            $obj->{$termo.'_pt'} ?:
            $termo;
    }
