-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 13-Abr-2020 às 20:53
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edcgroup`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `areas_de_atuacao`
--

CREATE TABLE `areas_de_atuacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `areas_de_atuacao`
--

INSERT INTO `areas_de_atuacao` (`id`, `texto_pt`, `texto_en`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>A EDC Group &eacute; uma Multinacional Brasileira com mais de 10 anos de experi&ecirc;ncia.</p>\r\n\r\n<p>Atua em toda a Am&eacute;rica Latina e EUA.</p>\r\n', '', 'img-areasatuacao-edc_202004132041054Lf6qmVgIW.jpg', NULL, '2020-04-13 20:41:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `nome`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 'Yazaki', 'client1_20200413204311px6Sa3kWQE.png', '2020-04-13 20:43:11', '2020-04-13 20:43:11'),
(2, 2, 'Benteler', 'client2_20200413204320nwGJQXPY7e.png', '2020-04-13 20:43:20', '2020-04-13 20:43:20'),
(3, 3, 'Continental', 'client3_2020041320432619hg1vXgKs.png', '2020-04-13 20:43:26', '2020-04-13 20:43:26'),
(4, 4, 'Faurecia', 'client4_20200413204334oX3oDCIqoC.png', '2020-04-13 20:43:34', '2020-04-13 20:43:34'),
(5, 5, 'Ericsson', 'client5_202004132043420rwC9X7OUg.png', '2020-04-13 20:43:42', '2020-04-13 20:43:42'),
(6, 6, 'AGCO', 'client6_202004132043483e4JsufylN.png', '2020-04-13 20:43:48', '2020-04-13 20:43:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'EDC Group', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco_pt`, `endereco_en`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, 'contato@edcgroup.com.br', '+55 11 3292 6444', 'SEDE · Rua José Bonifácio, 24 - 1º andar<br />Sé - São Paulo, SP · Brasil · 01003-900', '', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.5446474516048!2d-46.6367906850224!3d-23.548874684689107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59ab2647593f%3A0x207dd8a82eeee391!2zUi4gSm9zw6kgQm9uaWbDoWNpbywgMjQgLSBTw6ksIFPDo28gUGF1bG8gLSBTUCwgMDEwMDMtMDAw!5e0!3m2!1spt-BR!2sbr!4v1586624699297!5m2!1spt-BR!2sbr\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

CREATE TABLE `depoimentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `depoimento_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `depoimento_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `ordem`, `nome`, `depoimento_pt`, `depoimento_en`, `created_at`, `updated_at`) VALUES
(1, 1, 'Exemplo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis. Duis ultricies, sapien non porttitor lacinia, magna neque rutrum tortor, ac lobortis nisi sem in justo.', '', '2020-04-13 20:46:00', '2020-04-13 20:46:00'),
(2, 2, 'Exemplo 2', 'Vivamus in ultrices mauris. Nullam ac suscipit orci. Nunc ac purus dictum, commodo enim ut, fringilla augue. Sed a velit et enim pretium vulputate. In efficitur aliquam molestie. Aenean molestie neque vel dui cursus laoreet nec suscipit tellus. Aenean ut nisi lectus. Phasellus a tempor odio. Sed quis consectetur nisi, vel consequat lorem. Nam mi nisl, aliquam sit amet odio bibendum, vestibulum pellentesque dui.', '', '2020-04-13 20:46:05', '2020-04-13 20:46:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estrutura`
--

CREATE TABLE `estrutura` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `estrutura`
--

INSERT INTO `estrutura` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '1_202004132041452v9xxS4yHU.jpg', '2020-04-13 20:41:45', '2020-04-13 20:41:45'),
(2, 0, '2_202004132041486YT32xho6E.jpg', '2020-04-13 20:41:48', '2020-04-13 20:41:48'),
(3, 0, '3_20200413204151I5dCmgAtwE.jpg', '2020-04-13 20:41:51', '2020-04-13 20:41:51'),
(4, 0, '4_20200413204153I3PPAfLEfK.jpg', '2020-04-13 20:41:53', '2020-04-13 20:41:53'),
(5, 0, '5_20200413204156OcgkV91Veg.jpg', '2020-04-13 20:41:56', '2020-04-13 20:41:56'),
(6, 0, '6_20200413204159DOdnZhrGEv.jpg', '2020-04-13 20:41:59', '2020-04-13 20:41:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE `home` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `frase_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perfil_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_4_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_4_en` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_5_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `perfil_5_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `banner`, `frase_pt`, `frase_en`, `texto_pt`, `texto_en`, `imagem`, `perfil_1_pt`, `perfil_1_en`, `perfil_2_pt`, `perfil_2_en`, `perfil_3_pt`, `perfil_3_en`, `perfil_4_pt`, `perfil_4_en`, `perfil_5_pt`, `perfil_5_en`, `created_at`, `updated_at`) VALUES
(1, 'img-banner-home1_20200413203901faC0lw0nyq.jpg', 'Transformando pessoas<br />\r\nConectando neg&oacute;cios', '', '<h3>Que tipo de ajuda<br />\r\nvoc&ecirc; precisa para colocar<br />\r\nseus projetos em dia?</h3>\r\n\r\n<h2>Falta pessoal?<br />\r\nFalta conhecimento?</h2>\r\n\r\n<p><em>A <strong>EDC</strong> - com mais de 10 anos de experi&ecirc;ncia - criou um processo pr&oacute;prio para encontrar rapidamente o perfil profissional que voc&ecirc; precisa e executar o seu projeto.</em></p>\r\n', '', 'img-quadro-home_20200413203901rD9yoL3xX4.jpg', '<strong>Flexibilidade</strong> para se adaptar ao processo do cliente', '', '<strong>Agilidade</strong> na busca e sele&ccedil;&atilde;o dos perfis dos profissionais', '', 'Estrutura para atrair sempre os&nbsp;<strong>melhores talentos</strong>', '', 'Oferece atendimento do <strong>escopo completo</strong> do trabalho', '', 'Profissionais alocados no pr&oacute;prio cliente ou <strong>pacotes completos</strong> desenvolvidos no ambiente da EDC', '', NULL, '2020-04-13 20:39:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `linhas_de_atuacao`
--

CREATE TABLE `linhas_de_atuacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `linhas_de_atuacao`
--

INSERT INTO `linhas_de_atuacao` (`id`, `ordem`, `titulo_pt`, `titulo_en`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, 0, 'Exemplo', '', '<ul>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis.</li>\r\n	<li>Duis ultricies, sapien non porttitor lacinia, magna neque rutrum tortor, ac lobortis nisi sem in justo. Vivamus in ultrices mauris. Nullam ac suscipit orci.</li>\r\n</ul>\r\n', '', '2020-04-13 20:41:28', '2020-04-13 20:41:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `vagas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_do_candidato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_do_consultor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `links`
--

INSERT INTO `links` (`id`, `vagas`, `area_do_candidato`, `area_do_consultor`, `created_at`, `updated_at`) VALUES
(1, '#', '#', '#', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2020_04_10_023237_create_links_table', 1),
('2020_04_10_023850_create_termos_de_uso_table', 1),
('2020_04_10_023856_create_politica_de_privacidade_table', 1),
('2020_04_10_025309_create_quem_somos_table', 1),
('2020_04_10_031646_create_clientes_table', 1),
('2020_04_10_031713_create_estrutura_table', 1),
('2020_04_10_032848_create_areas_de_atuacao_table', 1),
('2020_04_10_033138_create_linhas_de_atuacao_table', 1),
('2020_04_10_190309_create_home_table', 1),
('2020_04_10_191420_create_depoimentos_table', 1),
('2020_04_10_211431_create_servicos_table', 1),
('2020_04_10_212614_create_servicos_introducao_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `politica_de_privacidade`
--

CREATE TABLE `politica_de_privacidade` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `politica_de_privacidade`
--

INSERT INTO `politica_de_privacidade` (`id`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `quem_somos`
--

CREATE TABLE `quem_somos` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_titulo_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_texto_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_texto_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_subtitulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_1_subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_titulo_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_texto_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_texto_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_subtitulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_2_subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_titulo_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_texto_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_texto_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_subtitulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_3_subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_4_titulo_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_4_titulo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_4_texto_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferencial_4_texto_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estrutura_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `estrutura_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `quem_somos`
--

INSERT INTO `quem_somos` (`id`, `texto_1_pt`, `texto_1_en`, `texto_2_pt`, `texto_2_en`, `texto_3_pt`, `texto_3_en`, `imagem_1`, `imagem_2_pt`, `imagem_2_en`, `diferencial_1_titulo_pt`, `diferencial_1_titulo_en`, `diferencial_1_texto_pt`, `diferencial_1_texto_en`, `diferencial_1_subtitulo_pt`, `diferencial_1_subtitulo_en`, `diferencial_2_titulo_pt`, `diferencial_2_titulo_en`, `diferencial_2_texto_pt`, `diferencial_2_texto_en`, `diferencial_2_subtitulo_pt`, `diferencial_2_subtitulo_en`, `diferencial_3_titulo_pt`, `diferencial_3_titulo_en`, `diferencial_3_texto_pt`, `diferencial_3_texto_en`, `diferencial_3_subtitulo_pt`, `diferencial_3_subtitulo_en`, `diferencial_4_titulo_pt`, `diferencial_4_titulo_en`, `diferencial_4_texto_pt`, `diferencial_4_texto_en`, `estrutura_pt`, `estrutura_en`, `created_at`, `updated_at`) VALUES
(1, '<h2>CONSULTORIA E OUTSOURCING DE SERVI&Ccedil;OS</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis. Duis ultricies, sapien non porttitor lacinia, magna neque rutrum tortor, ac lobortis nisi sem in justo. Vivamus in ultrices mauris. Nullam ac suscipit orci. Nunc ac purus dictum, commodo enim ut, fringilla augue. Sed a velit et enim pretium vulputate. In efficitur aliquam molestie.</p>\r\n', '', '<h2>POSI&Ccedil;&Atilde;O DA EDC NO MERCADO</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis.</p>\r\n', '', '<h2>MISS&Atilde;O</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis.</p>\r\n\r\n<h2>VIS&Atilde;O</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis.</p>\r\n\r\n<h2>VALORES</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis.</p>\r\n', '', 'img-edc-quemsomos_20200413204852fFFzhqmTZW.jpg', 'grafico-posicao-edc-mercado_20200413204853Y6f0toMcnZ.png', '', 'Capacidade<br />\r\n<strong>Consultiva</strong>', '', 'Entende o problema do cliente', '', 'FLEXIBILIDADE', '', 'Capacidade<br />\r\n<strong>Anal&iacute;tica e T&eacute;cnica</strong>', '', 'Traduz o problema em perfil profissional', '', 'MELHORES TALENTOS', '', 'Capacidade<br />\r\n<strong>Executiva</strong>', '', 'Encontra o profissional em tempo recorde', '', 'AGILIDADE', '', 'ATENDE O ESCOPO COMPLETO DE TRABALHO', '', 'Com recursos, computadores, software, rede, celular, etc.', '', '', '', NULL, '2020-04-13 20:48:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_superior_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_superior_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `ordem`, `slug`, `titulo_pt`, `titulo_en`, `subtitulo_pt`, `subtitulo_en`, `cor`, `capa`, `chamada_pt`, `chamada_en`, `texto_superior_pt`, `texto_superior_en`, `texto_1_pt`, `texto_1_en`, `texto_2_pt`, `texto_2_en`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'hunting', 'Hunting', '', '', '', '#76C04E', 'edc-servicos-hunting_202004132032301pORk6pc5V.jpg', 'Agilidade na localiza&ccedil;&atilde;o dos profissionais.<br />\r\n<strong>Supre a necessidade com o funcion&aacute;rio adequado.</strong><br />\r\nEfici&ecirc;ncia na coloca&ccedil;&atilde;o.', '', '- Agilidade na localiza&ccedil;&atilde;o dos profissionais.<br />\r\n<strong>- Supre a necessidade com o funcion&aacute;rio adequado.</strong><br />\r\n- Efici&ecirc;ncia na coloca&ccedil;&atilde;o.', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet. Morbi dignissim neque eget elit dapibus convallis. Duis ultricies, sapien non porttitor lacinia, magna neque rutrum tortor, ac lobortis nisi sem in justo.</p>\r\n', '', '<h2>NOSSO PROCESSO</h2>\r\n\r\n<h3>START</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<h3>ETAPA 1</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<h3>ETAPA 2</h3>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<ul>\r\n	<li>Donec blandit metus et nulla aliquam tristique. Praesent cursus molestie aliquet.</li>\r\n	<li>Morbi dignissim neque eget elit dapibus convallis. Duis ultricies, sapien non porttitor lacinia, magna neque rutrum tortor, ac lobortis nisi sem in justo.</li>\r\n</ul>\r\n', '', '20_20200413203230ODjJT0Q3nW.jpg', NULL, '2020-04-13 20:34:39'),
(2, 1, 'temporarios', 'Temporários', '', '', '', '#3CBB90', 'edc-servicos-temporarios_20200413203551994BMAxGUw.jpg', '.', '', '.', '', '<p>.</p>\r\n', '', '<p>.</p>\r\n', '', '', NULL, '2020-04-13 20:35:52'),
(3, 2, 'outsourcing', 'Outsourcing', '', '', '', '#3A96C9', 'edc-servicos-outsourcing_20200413203558n1xcXWNQZp.jpg', '.', '', '.', '', '<p>.</p>\r\n', '', '<p>.</p>\r\n', '', '', NULL, '2020-04-13 20:35:58'),
(4, 3, 'bpo', 'BPO', '', '(Business Process Outsourcing)', '', '#3B72C2', 'edc-servicos-bpo_202004132036069WhjsfJvfk.jpg', '.', '', '.', '', '<p>.</p>\r\n', '', '<p>.</p>\r\n', '', '', NULL, '2020-04-13 20:36:06'),
(5, 4, 'projetos-especiais', 'Projetos especiais', '', '', '', '#7154D3', 'edc-servicos-projetosespeciais_20200413203613of8SjOYIYl.jpg', '.', '', '.', '', '<p>.</p>\r\n', '', '<p>.</p>\r\n', '', '', NULL, '2020-04-13 20:36:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos_introducao`
--

CREATE TABLE `servicos_introducao` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos_introducao`
--

INSERT INTO `servicos_introducao` (`id`, `imagem`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '2_20200413202922Vz3mx6GA8Q.jpg', '<strong>NOSSO COMPROMISSO:</strong><br />\r\nAtua&ccedil;&atilde;o inovadora no desenvolvimento de pessoas com foco no neg&oacute;cio do cliente.', '', NULL, '2020-04-13 20:29:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `termos_de_uso`
--

CREATE TABLE `termos_de_uso` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `termos_de_uso`
--

INSERT INTO `termos_de_uso` (`id`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$Kw7PiynLftG5cmGkD2Stt.RYLXeIrcGVwr9yV4PdY/29MjUDMKg6O', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas_de_atuacao`
--
ALTER TABLE `areas_de_atuacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depoimentos`
--
ALTER TABLE `depoimentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estrutura`
--
ALTER TABLE `estrutura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linhas_de_atuacao`
--
ALTER TABLE `linhas_de_atuacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `politica_de_privacidade`
--
ALTER TABLE `politica_de_privacidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quem_somos`
--
ALTER TABLE `quem_somos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos_introducao`
--
ALTER TABLE `servicos_introducao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `termos_de_uso`
--
ALTER TABLE `termos_de_uso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas_de_atuacao`
--
ALTER TABLE `areas_de_atuacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `depoimentos`
--
ALTER TABLE `depoimentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `estrutura`
--
ALTER TABLE `estrutura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `linhas_de_atuacao`
--
ALTER TABLE `linhas_de_atuacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `politica_de_privacidade`
--
ALTER TABLE `politica_de_privacidade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quem_somos`
--
ALTER TABLE `quem_somos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `servicos_introducao`
--
ALTER TABLE `servicos_introducao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `termos_de_uso`
--
ALTER TABLE `termos_de_uso`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
